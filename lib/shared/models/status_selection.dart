import 'package:flutter/material.dart';

class StatusSelection<T> extends ChangeNotifier {
  Map<String, T> selectedOption = {};

  StatusSelection(this.selectedOption);

  void updateStatus(String key, T value) {
    selectedOption[key] = value;
    notifyListeners();
  }
}
