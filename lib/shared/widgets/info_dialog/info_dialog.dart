import 'package:flutter/material.dart';

class InfoDialog {
  static Future show({
    required BuildContext context,
    required Color backgroundColor,
    required List<Widget> children,
  }) {
    return showModalBottomSheet(
        context: context,
        backgroundColor: backgroundColor.withAlpha(0xFF),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        showDragHandle: true,
        builder: (context) => Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: children,
            )));
  }
}
