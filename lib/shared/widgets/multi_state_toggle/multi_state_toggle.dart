import 'package:flutter/material.dart';

class MultiStateToggle<T> extends StatelessWidget {
  final List<T?> states;
  final Map<T?, String> stateLabels;
  final T? selectedOption;
  final Function(T) onSelectionChange;
  final Color selectionColor;
  final Color backgroundColor;
  final bool enabled;

  const MultiStateToggle({
    super.key,
    required this.states,
    required this.stateLabels,
    required this.selectedOption,
    required this.onSelectionChange,
    required this.selectionColor,
    required this.backgroundColor,
    required this.enabled,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: backgroundColor,
      ),
      child: Row(
        children: states.map((status) {
          return GestureDetector(
            onTap: enabled ? () => onSelectionChange(status as T) : null,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: status == selectedOption
                    ? selectionColor
                    : Colors.transparent,
              ),
              child: Text(
                stateLabels[status].toString(),
                style: TextStyle(
                  fontSize: status == selectedOption ? 18 : 14,
                  fontWeight: status == selectedOption
                      ? FontWeight.bold
                      : FontWeight.normal,
                  color: Colors.white,
                ),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}
