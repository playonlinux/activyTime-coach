import 'package:flutter/material.dart';

class EmailLink extends StatelessWidget {
  final String? email;
  final void Function() onClick;
  const EmailLink({
    super.key,
    required this.email,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: () => onClick(), child: Text(email ?? ''));
  }
}
