import 'package:flutter/material.dart';

class SectionTitle extends StatelessWidget {
  final String title;
  const SectionTitle({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 0),
      decoration: const BoxDecoration(color: Colors.amber),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(title.toUpperCase(),
            style: const TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white))
      ]),
    );
  }
}
