import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/shared/widgets/multi_state_toggle/multi_state_toggle.dart';
import 'package:app_flutter/shared/models/status_selection.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CallStatusSelector extends StatelessWidget {
  final String id;
  final EventStatus status;
  final Color color;
  final Color backgroundColor;
  final void Function(CalleeStatus) onStatusChanged;

  const CallStatusSelector(
      {super.key,
      required this.id,
      required this.status,
      required this.color,
      required this.backgroundColor,
      required this.onStatusChanged});

  @override
  Widget build(BuildContext context) {
    return MultiStateToggle<CalleeStatus>(
        enabled: status == EventStatus.in_progress,
        states: CalleeStatus.values,
        stateLabels: CalleeStatus.values.groupFoldBy<CalleeStatus, String>(
            (element) => element, (previous, element) => element.toChar()),
        selectedOption: Provider.of<StatusSelection<CalleeStatus>>(context)
                .selectedOption[id] ??
            CalleeStatus.absent,
        onSelectionChange: (selectedOption) async {
          Provider.of<StatusSelection<CalleeStatus>>(context, listen: false)
              .updateStatus(id, selectedOption);
          onStatusChanged(selectedOption);
        },
        selectionColor: color,
        backgroundColor: backgroundColor);
  }
}
