import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/player/player_screen.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

extension TextEditingControllerExt on TextEditingController {
  void selectAll() {
    if (text.isEmpty) return;
    selection = TextSelection(baseOffset: 0, extentOffset: text.length);
  }
}

class CommentDialog {
  static Future show({
    required BuildContext context,
    required Player player,
    required EventType eventType,
    required String eventId,
    required Color backgroundColor,
    required Future<dynamic> Function(String) onConfirm,
    required Future<dynamic> Function() onDelete,
  }) {
    final ValueNotifier<bool> validateButton = ValueNotifier(false);
    final TextEditingController textFieldController = TextEditingController();
    return showModalBottomSheet(
        context: context,
        backgroundColor: backgroundColor.withAlpha(0xFF),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        showDragHandle: true,
        isScrollControlled: true,
        builder: (context) {
          Comment? comment = player.statistics.comments.firstWhereOrNull(
              (comment) =>
                  comment.eventType == eventType &&
                  comment.eventRef.id == eventId);

          if (textFieldController.text.isEmpty) {
            textFieldController.text = comment?.text ?? '';
            textFieldController.selectAll();
          }

          return Theme(
              data: Theme.of(context).copyWith(
                textSelectionTheme: TextSelectionThemeData(
                  selectionColor: Colors.white.withOpacity(0.2),
                  cursorColor: Colors.white,
                  selectionHandleColor: Colors.white,
                ),
              ),
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: ValueListenableBuilder<bool>(
                    valueListenable: validateButton,
                    builder: (context, events, _) {
                      return SingleChildScrollView(
                        child: Padding(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 0, 20.0, 20.0),
                          child:
                              Column(mainAxisSize: MainAxisSize.min, children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(children: [
                                    UserAvatar(
                                        url: player.profile.avatar,
                                        size: AvatarSize.extraSmall),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    TextLink(
                                      text:
                                          '${player.profile.firstName} ${player.profile.lastName}',
                                      style:
                                          const TextStyle(color: Colors.white),
                                      onClick: () => Navigator.pushNamed(
                                          context, '/player',
                                          arguments: PlayerScreenArguments(
                                              player: player)),
                                    ),
                                  ]),
                                  TextButton(
                                    style: ButtonStyle(
                                        foregroundColor:
                                            MaterialStateProperty.all(
                                                textFieldController
                                                            .text.isNotEmpty ||
                                                        comment?.text != null
                                                    ? Colors.white
                                                    : Colors.white24)),
                                    onPressed: (textFieldController
                                            .text.isNotEmpty)
                                        ? () async {
                                            await onConfirm(
                                                    textFieldController.text)
                                                .then((_) =>
                                                    Navigator.pop(context));
                                          }
                                        : (comment?.text != null)
                                            ? () async {
                                                await onDelete().then((_) =>
                                                    Navigator.pop(context));
                                              }
                                            : null,
                                    child: (textFieldController.text.isNotEmpty)
                                        ? const Text(
                                            'Commenter',
                                            textAlign: TextAlign.end,
                                          )
                                        : (comment?.text != null)
                                            ? const Text('Supprimer',
                                                textAlign: TextAlign.end)
                                            : const Text('Commenter',
                                                textAlign: TextAlign.end),
                                  )
                                ]),
                            const SizedBox(height: 20.0),
                            TextField(
                              style: const TextStyle(color: Colors.white),
                              maxLines: null,
                              autofocus: true,
                              onChanged: (value) {
                                validateButton.value = !validateButton.value;
                              },
                              controller: textFieldController,
                              decoration: const InputDecoration(
                                  enabledBorder: InputBorder.none,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white24)),
                                  focusColor: Colors.white,
                                  hintText: 'Ajouter un commentaire',
                                  hintStyle: TextStyle(color: Colors.white24)),
                            )
                          ]),
                        ),
                      );
                    }),
              ));
        });
  }
}
