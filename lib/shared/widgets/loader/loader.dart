import 'package:app_flutter/screens/settings/settings_screen.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:provider/provider.dart';

class LoadingStatus extends ChangeNotifier {
  bool isLoading;
  LoadingStatus(this.isLoading);

  void show() {
    isLoading = true;
    notifyListeners();
  }

  void hide() {
    isLoading = false;
    notifyListeners();
  }
}

class LoadingOverlay extends StatelessWidget {
  const LoadingOverlay(
      {super.key, required this.isLoading, required this.child, this.color});

  final Widget child;
  final bool isLoading;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        if (isLoading)
          const Opacity(
            opacity: 0.5,
            child: ModalBarrier(dismissible: false, color: Colors.black),
          ),
        if (isLoading)
          Center(
              child: SizedBox(
                  width: 70,
                  height: 70,
                  child: LoadingIndicator(
                    indicatorType: Indicator.lineScale,
                    colors: [
                      color ??
                          Provider.of<ThemeProvider>(context)
                              .currentScheme
                              .primary
                    ],
                    backgroundColor: Colors.transparent,
                  ))),
      ],
    );
  }
}