import 'package:flutter/material.dart';

enum AvatarSize { extraSmall, small, medium, large }

class UserAvatar extends StatelessWidget {
  const UserAvatar(
      {super.key, required this.url, this.size = AvatarSize.small});

  final String? url;
  final AvatarSize size;

  @override
  Widget build(BuildContext context) {
    return url == null
        ? SizedBox(
            width: switch (size) {
              AvatarSize.extraSmall => 20,
              AvatarSize.small => 40,
              AvatarSize.medium => 128,
              AvatarSize.large => 240,
            },
            height: switch (size) {
              AvatarSize.extraSmall => 20,
              AvatarSize.small => 40,
              AvatarSize.medium => 128,
              AvatarSize.large => 240,
            })
            // child: const LoadingIndicator(
            //   indicatorType: Indicator.lineScale,
            //   colors: [Colors.white],
            //   backgroundColor: Colors.transparent,
            // ))
        : CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: switch (size) {
              AvatarSize.extraSmall => 20,
              AvatarSize.small => 20,
              AvatarSize.medium => 70,
              AvatarSize.large => 240,
            },
            backgroundImage: NetworkImage(url!, scale: 3),        
            );
  }
}
