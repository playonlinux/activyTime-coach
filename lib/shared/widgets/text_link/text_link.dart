import 'package:flutter/material.dart';

class TextLink extends StatelessWidget {
  final String text;
  final void Function() onClick;
  final TextStyle? style;
  const TextLink(
      {super.key, required this.text, required this.onClick, this.style});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () => onClick(),
        child: Text(
          text,
          style: style ?? const TextStyle(),
          textAlign: TextAlign.start,
        ));
  }
}
