import 'package:flutter/material.dart';

class PhoneLink extends StatelessWidget {
  final String? phone;
  final void Function() onClick;
  const PhoneLink({
    super.key,
    required this.phone,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: () => onClick(), child: Text(phone ?? ''));
  }
}
