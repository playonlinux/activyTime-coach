import 'dart:core';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:json_annotation/json_annotation.dart';
//import 'package:json_annotation/json_annotation.dart';

// import 'package:enum_to_string/enum_to_string.dart';

class UserProfile {
  String firstName;
  String lastName;
  DateTime dateOfBirth;
  String avatar;

  UserProfile(
      {required this.firstName,
      required this.lastName,
      required this.dateOfBirth,
      required this.avatar});

  UserProfile.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            firstName: snapshot['firstName'],
            lastName: snapshot['lastName'],
            dateOfBirth:
                DateTime.fromMillisecondsSinceEpoch(snapshot['dateOfBirth']),
            avatar: snapshot['avatar'] ??
                'https://resources.evertonfc.com/players/side/480x540/p212319.png');
}

class UserDetails {
  String email = "";
  String phone = "";

  UserDetails({required this.email, required this.phone});

  UserDetails.fromFirestore(Map<String, dynamic> snapshot)
      : this(email: snapshot['email'], phone: snapshot['phone']);
}

class Responsible {
  String id;
  UserProfile profile;
  UserDetails details;
  List<DocumentReference> playerRefs = List.empty();

  Responsible(
      {required this.id,
      required this.profile,
      required this.details,
      required this.playerRefs});

  Responsible.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            id: snapshot['id'],
            profile: UserProfile.fromFirestore(snapshot['profile']),
            details: UserDetails.fromFirestore(snapshot['details']),
            playerRefs: (snapshot['playerRefs'] as List)
                .map((e) => e as DocumentReference)
                .toList());
}

class Player {
  String id;
  UserProfile profile;
  UserDetails details;
  // val responsibles: List<Responsible>,
  // val group: Group,
  List<DocumentReference> responsibleRefs;
  DocumentReference groupRef;
  PlayerStatistics statistics;

  Player(
      {required this.id,
      required this.profile,
      required this.details,
      required this.responsibleRefs,
      required this.groupRef,
      required this.statistics});

  static Player fromFirestore(Map<String, dynamic> snapshot) {
   // print(snapshot);
    return Player(
        id: snapshot['id'],
        profile: UserProfile.fromFirestore(snapshot['profile']),
        details: UserDetails.fromFirestore(snapshot['details']),
        responsibleRefs: (snapshot['responsibleRefs'] as List)
            .map((e) => e as DocumentReference)
            .toList(),
        groupRef: snapshot['groupRef'],
        statistics: PlayerStatistics.fromFirestore(snapshot['statistics']));
  }
}

class PlayerStatistics {
  Map<String, PlayerMonthlyStatistics> stats = {};
  List<Comment> comments = List.empty();

  PlayerStatistics({required this.stats, required this.comments});

  static PlayerStatistics fromFirestore(Map<String, dynamic> snapshot) {
  //  print(snapshot);
    return PlayerStatistics(
        stats: (snapshot['stats'] != null)
            ? (snapshot['stats'] as Map<String, dynamic>).map((key, value) =>
                MapEntry(key, PlayerMonthlyStatistics.fromFirestore(value)))
            : {},
        comments: (snapshot['comments'] != null)
            ? (snapshot['comments'] as List)
                .map((comment) => Comment.fromFirestore(comment))
                .toList()
            : []);
  }
}

class PlayerMonthlyStatistics {
  PlayerMonthlyMatchStatistics matches;
  PlayerMonthlyTrainingStatistics trainings;

  PlayerMonthlyStatistics({required this.matches, required this.trainings});

  PlayerMonthlyStatistics.fromFirestore(Map<String, dynamic> snapshot)
      : this(
          matches:
              PlayerMonthlyMatchStatistics.fromFirestore(snapshot['matches']),
          trainings: PlayerMonthlyTrainingStatistics.fromFirestore(
              snapshot['trainings']),
        );

  static Map<String, dynamic> toFirestore(PlayerMonthlyStatistics statistics) {
    return {
      'matches': PlayerMonthlyMatchStatistics.toFirestore(statistics.matches),
      'trainings':
          PlayerMonthlyTrainingStatistics.toFirestore(statistics.trainings)
    };
  }
}

class PlayerMonthlyTrainingStatistics {
  int nbPresent = 0;
  int nbAbsent = 0;
  int nbLate = 0;
  int nbInjured = 0;
  int nbTrainings = 0;

  PlayerMonthlyTrainingStatistics(
      {this.nbPresent = 0,
      this.nbAbsent = 0,
      this.nbLate = 0,
      this.nbInjured = 0,
      this.nbTrainings = 0});

  PlayerMonthlyTrainingStatistics.fromFirestore(Map<String, dynamic> snapshot)
      : this(
          nbPresent: snapshot['nbPresent'] ?? 0,
          nbAbsent: snapshot['nbAbsent'] ?? 0,
          nbLate: snapshot['nbLate'] ?? 0,
          nbInjured: snapshot['nbInjured'] ?? 0,
          nbTrainings: snapshot['nbTrainings'] ?? 0,
        );

  static Map<String, dynamic> toFirestore(
      PlayerMonthlyTrainingStatistics trainings) {
    return {
      'nbPresent': trainings.nbPresent,
      'nbAbsent': trainings.nbAbsent,
      'nbLate': trainings.nbLate,
      'nbInjured': trainings.nbInjured,
      'nbTrainings': trainings.nbTrainings
    };
  }
}

class PlayerMonthlyMatchStatistics {
  /*var nbStartAtPosition: Int = 0,
	var nbStartOnBench: Int = 0,*/

  int nbPresent = 0;
  int nbAbsent = 0;
  int nbLate = 0;
  int nbCalled = 0;
  int nbNotCalled = 0;
  int nbInjured = 0;
  int nbMatches = 0;

  PlayerMonthlyMatchStatistics(
      {this.nbPresent = 0,
      this.nbAbsent = 0,
      this.nbLate = 0,
      this.nbInjured = 0,
      this.nbCalled = 0,
      this.nbNotCalled = 0,
      this.nbMatches = 0});

  PlayerMonthlyMatchStatistics.fromFirestore(Map<String, dynamic> snapshot)
      : this(
          nbPresent: snapshot['nbPresent'] ?? 0,
          nbAbsent: snapshot['nbAbsent'] ?? 0,
          nbLate: snapshot['nbLate'] ?? 0,
          nbInjured: snapshot['nbInjured'] ?? 0,
          nbCalled: snapshot['nbCalled'] ?? 0,
          nbNotCalled: snapshot['nbNotCalled'] ?? 0,
          nbMatches: snapshot['nbMatches'] ?? 0,
        );

  static Map<String, dynamic> toFirestore(
      PlayerMonthlyMatchStatistics matches) {
    return {
      'nbPresent': matches.nbPresent,
      'nbAbsent': matches.nbAbsent,
      'nbLate': matches.nbLate,
      'nbInjured': matches.nbInjured,
      'nbCalled': matches.nbCalled,
      'nbNotCalled': matches.nbNotCalled,
      'nbMatches': matches.nbMatches,
    };
  }

  List<String> keys() {
    return [
      'nbPresent',
      'nbAbsent',
      'nbLate',
      'nbInjured',
      'nbCalled',
      'nbNotCalled',
      'nbMatches'
    ];
  }

  List<int> values() {
    return [
      nbPresent,
      nbAbsent,
      nbLate,
      nbInjured,
      nbCalled,
      nbNotCalled,
      nbMatches
    ];
  }
}

class Coach {
  String id = "";
  UserProfile profile;
  UserDetails details;
  List<DocumentReference> categoryRefs = List.empty();
  List<DocumentReference> groupRefs = List.empty();

  Coach(
      {required this.id,
      required this.profile,
      required this.details,
      required this.categoryRefs,
      required this.groupRefs});

  static Coach fromFirestore(Map<String, dynamic> snapshot) {
    return Coach(
      id: snapshot['id'],
      profile: UserProfile.fromFirestore(snapshot['profile']),
      details: UserDetails.fromFirestore(snapshot['details']),
      categoryRefs: (snapshot['categoryRefs'] as List)
          .map((e) => e as DocumentReference)
          .toList(),
      groupRefs: (snapshot['groupRefs'] as List)
          .map((e) => e as DocumentReference)
          .toList(),
    );
  }
}

enum GroupLevel implements Comparable<GroupLevel> {
  A('A'),
  B('B'),
  C('C'),
  D('D'),
  NC('N/C');

  final String level;
  const GroupLevel(this.level);

  @override
  String toString() {
    return name.toUpperCase();
  }

  @override
  int compareTo(GroupLevel other) {
    return name.compareTo(other.name);
  }
}

// extension EnumTransform on List {
//   String enumToString<T>(T enumValue) {
//     return enumValue;
//     // if (value == null || (isEmpty)) return null;
//     // var occurence = singleWhere(
//     //     (enumItem) => enumItem.toString() == value.toString(),
//     //     orElse: () => null);
//     // if (occurence == null) return null;
//     // return occurence.toString().split('.').last;
//   }

//   T stringToEnum<T>(String value) {
//     return firstWhere((type) => type.toString().split('.').last == value,
//         orElse: () => null);
//   }
// }

enum CategoryLevel {
  u11('U11'),
  u12('U12'),
  u13('U13'),
  u14('U14'),
  u15('U15'),
  u16('U16'),
  u17('U17'),
  u18('U18');

  final String level;
  const CategoryLevel(this.level);

  @override
  String toString() {
    return name.toUpperCase();
  }
}

enum EventType {
  training('TRAINING'),
  match('MATCH');

  final String type;
  const EventType(this.type);

  @override
  String toString() {
    return name.toUpperCase();
  }

  String toLabel() {
    switch (this) {
      case training:
        return 'Entraînement';
      case EventType.match:
        return 'Match';
    }
  }
}

enum EventStatus {
  idle('IDLE'),
  in_progress('IN_PROGRESS'),
  ended('ENDED'),
  cancelled('CANCELLED');

  final String status;
  const EventStatus(this.status);

  @override
  String toString() {
    return name.toUpperCase();
  }

  static EventStatus getNextStatus(EventStatus s) {
    switch (s) {
      case idle:
        return in_progress;
      case in_progress:
        return ended;
      default:
        return s;
    }
  }

  String getButtonLabelByStatus() {
    switch (this) {
      case idle:
        return 'DÉBUTER LA SÉANCE';
      case in_progress:
        return 'TERMINER LA SÉANCE';
      case ended:
        return 'TERMINÉ';
      case cancelled:
        return 'ANNULÉ';
    }
  }

  String toLabel() {
    switch (this) {
      case idle:
        return 'EN ATTENTE';
      case in_progress:
        return 'EN COURS';
      case ended:
        return 'TERMINÉ';
      case cancelled:
        return 'ANNULÉ';
    }
  }
}

class Category {
  String id = "";
  CategoryLevel label = CategoryLevel.u11;
  List<DocumentReference> coachRefs = List.empty();
  List<DocumentReference> groupRefs = List.empty();

  Category(
      {required this.id,
      required this.label,
      required this.coachRefs,
      required this.groupRefs});

  Category.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            id: snapshot['id'],
            label: CategoryLevel.values.byName(snapshot['label'].toLowerCase()),
            coachRefs: (snapshot['coachRefs'] as List)
                .map((e) => e as DocumentReference)
                .toList(),
            groupRefs: (snapshot['groupRefs'] as List)
                .map((e) => e as DocumentReference)
                .toList());
}

class Group {
  String id = "";
  GroupLevel level = GroupLevel.A;
  DocumentReference categoryRef;
  List<DocumentReference> coachRefs = List.empty(growable: true);
  List<DocumentReference> playerRefs = List.empty(growable: true);

  Group(
      {required this.id,
      required this.level,
      required this.categoryRef,
      required this.coachRefs,
      required this.playerRefs});

  Group.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            id: snapshot['id'],
            level: GroupLevel.values.byName(snapshot['level']),
            categoryRef: snapshot['categoryRef'],
            coachRefs: (snapshot['coachRefs'] as List)
                .map((e) => e as DocumentReference)
                .toList(),
            playerRefs: (snapshot['playerRefs'] as List)
                .map((e) => e as DocumentReference)
                .toList());
}

enum CalleeStatus {
  absent('ABSENT'),
  present('PRESENT'),
  late('LATE'),
  injured('INJURED');

  final String status;
  const CalleeStatus(this.status);

  @override
  String toString() {
    return name.toUpperCase();
  }

  String toChar() {
    switch (this) {
      case absent:
        return 'A';
      case present:
        return 'P';
      case late:
        return 'R';
      case injured:
        return 'B';
    }
  }
}

class Callee {
  DocumentReference playerRef;
  CalleeStatus status = CalleeStatus.injured;
  String reason = '';

  Callee({required this.playerRef, required this.status, required this.reason});

  Callee.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            playerRef: snapshot['playerRef'],
            status:
                CalleeStatus.values.byName(snapshot['status'].toLowerCase()),
            reason: snapshot['reason'] ?? '');

  static Map<String, dynamic> toFirestore(Callee callee) {
    return {
      'playerRef': callee.playerRef,
      'status': callee.status.toString(),
      'reason': callee.reason
    };
  }
}

class Comment {
  EventType eventType = EventType.match;
  DocumentReference eventRef;
  String text = '';

  Comment(
      {required this.eventType, required this.eventRef, required this.text});

  Comment.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            eventType:
                EventType.values.byName(snapshot['eventType'].toLowerCase()),
            eventRef: snapshot['eventRef'],
            text: snapshot['text']);

  static Map<String, dynamic>? toFirestore(Comment? comment) {
    return comment != null
        ? {
            'eventType': comment.eventType.toString(),
            'eventRef': comment.eventRef,
            'text': comment.text
          }
        : null;
  }
}

class Meeting {
  String point = '';
  DateTime startTime = DateTime.now();
  DateTime endTime = DateTime.now();

  Meeting(
      {required this.point, required this.startTime, required this.endTime});

  static Meeting fromFirestore(Map<String, dynamic> snapshot) {
    return Meeting(
        point: snapshot['point'],
        startTime: DateTime.fromMillisecondsSinceEpoch(snapshot['startTime']),
        endTime: DateTime.fromMillisecondsSinceEpoch(snapshot['endTime']));
  }

  static Map<String, dynamic> toFirestore(Meeting meeting) {
    return {
      'point': meeting.point,
      'startTime': meeting.startTime.millisecondsSinceEpoch,
      'endTime': meeting.endTime.millisecondsSinceEpoch,
    };
  }
}

enum MatchType {
  cup('CUP'),
  friendly('FRIENDLY'),
  championship('CHAMPI0NSHIP');

  final String type;
  const MatchType(this.type);
}

// @Suppress("SpellCheckingInspection")
typedef OpponentName = String;
//{
// PSG_ACADEMY("Paris Saint-Germain Academy Hauts-de-Seine"),
// AS_MEUDON("AS Meudon Football"),
// FC_ISSY("FC Issy-les-Moulineaux"),
// FC_LA_GARENNE("FC Garenne-Colombes"),
// CO_LES_ULIS("CO Les Ulis"),
// AS_CHATENAY_MALABRY("AS Chatenay-Malabry"),
// RUEIL_AC("Rueil Athletic Club"),
// ACBB("AC Boulogne-Billancourt"),
// AS_FONTENAY_AUX_ROSES("AS Fontenay-aux-Roses"),
// AS_CLAMART_FOOTBALL("AS Clamart Football"),
// FC_SURESNES("Suresnes Football Club"),
// USMT("US Métro Transport (USMT)"),
// AS_NEUILLY_SUR_SEINE("AS Neuilly-sur-Seine"),
// AS_MEUDON_LA_FORET("AS Meudon-la-Forêt"),
// AS_NANTERRE("AS Nanterre"),
// AS_BOURG_LA_REINE("AS Bourg-la-Reine"),
// US_MALAKOFF("US Malakoff"),
// ES_NANTERRE("ES Nanterre"),
// AS_VILLE_D_AVRAY("AS Ville-d'Avray"),
// AS_BAGNEUX("AS Bagneux"),
// AS_SAINT_CLOUD("AS Saint-Cloud"),
// AS_LA_ROCHETTE("AS La Rochette"),
// AC_PARIS("Club Athlétique de Paris"),
// FC_PUTEAUX("Puteaux Football Club")
//}

class Opponent {
  OpponentName name = '';

  Opponent({required this.name});

  factory Opponent.fromFirestore(Map<String, dynamic> snapshot) {
    return Opponent(name: snapshot['name']);
  }
}

class Sheet {
  String id = '';
  List<DocumentReference> calledPlayerRefs = List.empty(growable: true);
  List<DocumentReference> notCalledPlayerRefs = List.empty(growable: true);

  Sheet(
      {required this.id,
      required this.calledPlayerRefs,
      required this.notCalledPlayerRefs});

  static Sheet fromFirestore(Map<String, dynamic> snapshot) {
    return Sheet(
        id: snapshot['id'],
        calledPlayerRefs: (snapshot['calledPlayerRefs'] as List)
            .map((e) => e as DocumentReference)
            .toList(),
        notCalledPlayerRefs: (snapshot['notCalledPlayerRefs'] as List)
            .map((e) => e as DocumentReference)
            .toList());
  }

  static Map<String, dynamic> toFirestore(Sheet sheet) {
    return {
      'id': sheet.id,
      'calledPlayerRefs': sheet.calledPlayerRefs,
      'notCalledPlayerRefs': sheet.notCalledPlayerRefs,
    };
  }
}

class EventColor {
  Color background;
  Color topBar;

  EventColor({required this.background, required this.topBar});

  EventColor.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            background: Color(
                int.parse(snapshot['background'].substring(1, 9), radix: 16) +
                    0xFF000000),
            topBar: Color(
                int.parse(snapshot['topBar'].substring(1, 9), radix: 16) +
                    0xFF000000));

  static Map<String, dynamic> toFirestore(EventColor color) {
    return {
      'background': '#${color.background.value.toRadixString(16)}',
      'topBar': '#${color.topBar.value.toRadixString(16)}',
    };
  }
}

abstract class CalendarEvent {
  String id = '';
  EventType type = EventType.match;
  EventStatus status = EventStatus.cancelled;
  String eventId = '';
  late DocumentReference groupRef;
  late DocumentReference coachRef;
  late Meeting meeting;
  List<Callee> call = List.empty(growable: true);
  late Comment? comment;
  late EventColor color;

  static List<Callee> getCall(List<dynamic>? call) {
    return call?.map((callee) => Callee.fromFirestore(callee)).toList() ?? [];
  }
}

//@JsonSerializable(explicitToJson: true)
class Training implements CalendarEvent {
  @override
  String id = '';
  @override
  EventType type = EventType.match;
  @override
  EventStatus status = EventStatus.cancelled;
  @override
  String eventId = '';
  @override
  DocumentReference groupRef;
  @override
  DocumentReference coachRef;
  @override
  Meeting meeting;
  @override
  List<Callee> call = List.empty(growable: true);
  @override
  Comment? comment;
  @override
  EventColor color;
  Training(
      {required this.id,
      required this.type,
      required this.status,
      required this.eventId,
      required this.groupRef,
      required this.coachRef,
      required this.meeting,
      required this.call,
      required this.comment,
      required this.color});

  Training.fromFirestore(Map<String, dynamic> snapshot)
      : this(
          id: snapshot['id'],
          type: EventType.values.byName(snapshot['type'].toLowerCase()),
          status: EventStatus.values.byName(snapshot['status'].toLowerCase()),
          eventId: snapshot['eventId'],
          groupRef: snapshot['groupRef'],
          coachRef: snapshot['coachRef'],
          meeting: Meeting.fromFirestore(snapshot['meeting']),
          call: CalendarEvent.getCall(snapshot['call']),
          comment: snapshot['comment'] != null
              ? Comment.fromFirestore(snapshot['comment'])
              : null,
          color: EventColor.fromFirestore(snapshot['color']),
        );

  static Map<String, dynamic> toFirestore(Training training) {
    return {
      'id': training.id,
      'type': training.type.toString(),
      'status': training.status.toString(),
      'eventId': training.eventId,
      'groupRef': training.groupRef,
      'coachRef': training.coachRef,
      'meeting': Meeting.toFirestore(training.meeting),
      'call': training.call
          .map((Callee callee) => Callee.toFirestore(callee))
          .toList(),
      'comment': Comment.toFirestore(training.comment),
      'color': EventColor.toFirestore(training.color)
    };
  }
}

class Match implements CalendarEvent {
  @override
  String id = '';
  @override
  EventType type = EventType.match;
  @override
  EventStatus status = EventStatus.cancelled;
  @override
  String eventId = '';
  @override
  DocumentReference groupRef;
  @override
  DocumentReference coachRef;
  @override
  Meeting meeting;
  @override
  List<Callee> call = List.empty(growable: true);
  @override
  Comment? comment;
  @override
  EventColor color;

  Opponent opponent;
  MatchType matchType = MatchType.friendly;
  Sheet sheet;

  Match(
      {required this.id,
      required this.type,
      required this.status,
      required this.eventId,
      required this.groupRef,
      required this.coachRef,
      required this.meeting,
      required this.call,
      required this.comment,
      required this.color,
      required this.opponent,
      required this.matchType,
      required this.sheet});

  Match.fromFirestore(Map<String, dynamic> snapshot)
      : this(
            id: snapshot['id'],
            type: EventType.values.byName(snapshot['type'].toLowerCase()),
            status: EventStatus.values.byName(snapshot['status'].toLowerCase()),
            eventId: snapshot['eventId'],
            groupRef: snapshot['groupRef'],
            coachRef: snapshot['coachRef'],
            meeting: Meeting.fromFirestore(snapshot['meeting']),
            call: CalendarEvent.getCall(snapshot['call']),
            comment: snapshot['comment'],
            color: EventColor.fromFirestore(snapshot['color']),
            opponent: Opponent.fromFirestore(snapshot['opponent']),
            matchType:
                MatchType.values.byName(snapshot['matchType'].toLowerCase()),
            sheet: Sheet.fromFirestore(snapshot['sheet']));

  static Map<String, dynamic> toFirestore(Match match) {
    return {
      'id': match.id,
      'type': match.type.toString(),
      'status': match.status.toString(),
      'eventId': match.eventId,
      'groupRef': match.groupRef,
      'coachRef': match.coachRef,
      'meeting': Meeting.toFirestore(match.meeting),
      'call': match.call
          .map((Callee callee) => Callee.toFirestore(callee))
          .toList(),
      'comment': Comment.toFirestore(match.comment),
      'color': EventColor.toFirestore(match.color),
    };
  }
}

class UserRef {
  String userId = '';
  DocumentReference userRef;

  UserRef({required this.userId, required this.userRef});

  static UserRef fromFirestore(Map<String, dynamic> snapshot) {
    return UserRef(userId: snapshot['userID'], userRef: snapshot['userRef']);
  }
}
