import 'package:cloud_firestore/cloud_firestore.dart';


class DatabaseService {
  final CollectionReference coaches = FirebaseFirestore.instance.collection('coaches');
  final CollectionReference users = FirebaseFirestore.instance.collection('users');
  final CollectionReference trainings = FirebaseFirestore.instance.collection('trainings');
  final CollectionReference matches = FirebaseFirestore.instance.collection('matches');
  final CollectionReference categories = FirebaseFirestore.instance.collection('categories');
  final CollectionReference groups = FirebaseFirestore.instance.collection('groups');
  final CollectionReference players = FirebaseFirestore.instance.collection('players');
  final CollectionReference responsibles = FirebaseFirestore.instance.collection('responsibles');


  // Future<List<T>> get<T>(Query query) async {
  //   return await query.get().then((value) => value.docs.map((e) => ['']).toList() )
  // }
}