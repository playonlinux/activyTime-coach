import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;


  Future<User?> signInAnon() async {
    try {
      UserCredential authResult = await _auth.signInAnonymously();
      User? user = authResult.user;
      return user;
    
    } catch (e) {
      print(e.toString());
      return null;
    }
  }


  Future<User?> signInWithEmailAndPassword(String email, String password) async {
    UserCredential credentials = await _auth.signInWithEmailAndPassword(email: email, password: password);
    return credentials.user;
  }

  Future<void> signOut() async {
    return _auth.signOut();
  }
}
