import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/auth.dart';
import 'package:app_flutter/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserRepository {
  final AuthService _authService = AuthService();
  final DatabaseService _databaseService = DatabaseService();

  Future<User?> signInAnon() async {
    return _authService.signInAnon();
  }

  Future<User?> signInWithEmailAndPassword(
      String email, String password) async {
    return _authService.signInWithEmailAndPassword(email, password);
  }

  Future<void> signOut() async {
    return _authService.signOut();
  }

  Future<UserRef> getUserRefByUid(String uid) async {
    return (await _databaseService.users
            .where("userID", isEqualTo: uid)
            .withConverter<UserRef>(
                fromFirestore: (snapshot, _) =>
                    UserRef.fromFirestore(snapshot.data()!),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((snapshot) => snapshot.data())
        .first;
  }
}
