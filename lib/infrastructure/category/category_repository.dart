import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';

class CategoryRepository {
  final DatabaseService _databaseService = DatabaseService();

  Future<Category> getCategoryByRef(DocumentReference categoryRef) async {
    return (await categoryRef
            .withConverter(
                fromFirestore: (snapshot, _) => Category.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .data()!;
  }

  Future<List<Category>> findCategoriesByRefs(
      List<DocumentReference> categoryRefs) async {
    List<Category> categories = [];
    await Future.forEach(categoryRefs, (categoryRef) async {
      categories.add(await getCategoryByRef(categoryRef));
    });

    return categories;
  }

  Future<List<Category>> findCategoriesByIds(List<String> categoryIds) async {
    List<Category> categories = [];
    await Future.forEach(categoryIds.slices(30), (ids) async {
      List<Category> p = (await _databaseService.categories
              .where(FieldPath.documentId, whereIn: ids)
              .withConverter(
                  fromFirestore: (snapshot, _) => Category.fromFirestore(
                      {...snapshot.data()!, 'id': snapshot.id}),
                  toFirestore: (value, options) => {})
              .get())
          .docs
          .map((e) => e.data())
          .toList();
      categories.addAll(p);
    });
    return categories;
  }
}
