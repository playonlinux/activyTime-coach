import 'dart:core';

import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';

class PlayerRepository {
  final DatabaseService _databaseService = DatabaseService();

  Future<List<Player>> getPlayersByCoachRef(
      DocumentReference<Object?> coachRef) async {
    return (await _databaseService.players
            .where('coachRef', isEqualTo: coachRef)
            .withConverter<Player>(
                fromFirestore: (snapshot, _) => Player.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((element) => element.data())
        .toList();
  }

  Future<List<Player>> findPlayersByIds(List<String> playerIds) async {
    List<Player> players = [];
    await Future.forEach(playerIds.slices(30), (ids) async {
      List<Player> p = (await _databaseService.players
              .where(FieldPath.documentId, whereIn: ids)
              .withConverter(
                  fromFirestore: (snapshot, _) => Player.fromFirestore(
                      {...snapshot.data()!, 'id': snapshot.id}),
                  toFirestore: (value, options) => {})
              .get())
          .docs
          .map((e) => e.data())
          .toList();
      players.addAll(p);
    });
    return players;
  }

  DocumentReference<Object?> getDocRefById(String playerId) {
    return _databaseService.players.doc(playerId);
  }

  Future<void> updateComment(Player player) async {
    Map<Object, Object?> properties = {
      'statistics': {
        'comments': player.statistics.comments
            .map((comment) => Comment.toFirestore(comment))
            .toList()
      }
    };
    await getDocRefById(player.id).update(properties);
  }

  Future<void> updateStats(Player player) async {
    Map<Object, Object?> properties = {
      'statistics': {
        'stats': player.statistics.stats.map((key, value) =>
            MapEntry(key, PlayerMonthlyStatistics.toFirestore(value)))
      }
    };

    await getDocRefById(player.id).update(properties);
  }
}
