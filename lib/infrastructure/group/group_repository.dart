import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';

class GroupRepository {
  final DatabaseService _databaseService = DatabaseService();

  Future<Group> getGroupByRef(DocumentReference groupRef) async {
    return (await groupRef
            .withConverter(
                fromFirestore: (snapshot, _) => Group.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .data()!;
  }

  Future<List<Group>> findGroupsByIdsSortedByGroupLevel(
      List<String> groupIds) async {
    return (await _databaseService.groups
            .where(FieldPath.documentId, whereIn: groupIds)
            .withConverter(
                fromFirestore: (snapshot, _) => Group.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((e) => e.data())
        .toList()
        .sortedBy<GroupLevel>((Group group) => group.level);
  }

  Future<Group> getGroupById(String groupId) async {
    return getGroupByRef(_getDocRefById(groupId));
  }

  DocumentReference<Object?> _getDocRefById(String matchId) {
    return _databaseService.groups.doc(matchId);
  }
}
