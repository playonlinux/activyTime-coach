import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TrainingRepository {
  final DatabaseService _databaseService = DatabaseService();

  Future<Category?> getCategoryByRef(DocumentReference categoryRef) async {
    return (await categoryRef
            .withConverter(
                fromFirestore: (snapshot, _) => Category.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .data();
  }

  Future<List<Training>> getTrainingsByCoachRef(
      DocumentReference<Object?> coachRef) async {
    return (await _databaseService.trainings
            .where('coachRef', isEqualTo: coachRef)
            .withConverter<Training>(
                fromFirestore: (snapshot, _) => Training.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((element) => element.data())
        .toList();
  }

  Future<void> updateStatus(Training training) async {
    Map<Object, Object?> properties = {'status': '${training.status}'};
    await getDocRefById(training.id).update(properties);
  }

  DocumentReference<Object?> getDocRefById(String trainingId) {
    return _databaseService.trainings.doc(trainingId);
  }

  Future<void> updateCall(Training training) async {
    Map<Object, Object?> properties = {
      'call': training.call
          .map((Callee callee) => Callee.toFirestore(callee))
          .toList()
    };
    await getDocRefById(training.id).update(properties);
  }
}
