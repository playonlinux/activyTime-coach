import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ResponsibleRepository {
  final DatabaseService _databaseService = DatabaseService();

  Future<Coach> getCoachByRef(DocumentReference coachRef) async {
    return (await coachRef
            .withConverter(
                fromFirestore: (snapshot, _) => Coach.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .data()!;
  }

  Future<List<Responsible>> findResponsiblesByIds(
      List<String> responsibleIds) async {
    return (await _databaseService.responsibles
            .where(FieldPath.documentId, whereIn: responsibleIds)
            .withConverter(
                fromFirestore: (snapshot, _) => Responsible.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((e) => e.data())
        .toList();
  }
}
