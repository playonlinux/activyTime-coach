import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MatchRepository {
  final DatabaseService _databaseService = DatabaseService();

  Future<List<Match>> getMatchsByCoachRef(
      DocumentReference<Object?> coachRef) async {
    return (await _databaseService.matches
            .where('coachRef', isEqualTo: coachRef)
            .withConverter<Match>(
                fromFirestore: (snapshot, _) => Match.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((element) => element.data())
        .toList();
  }

  Future<Match> getMatchById(String matchId) async {
    return (await getDocRefById(matchId)
            .withConverter(
                fromFirestore: (snapshot, _) => Match.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .data()!;
  }

  Future<void> updateStatus(Match match) async {
    Map<Object, Object?> properties = {'status': '${match.status}'};
    await getDocRefById(match.id).update(properties);
  }

  DocumentReference<Object?> getDocRefById(String matchId) {
    return _databaseService.matches.doc(matchId);
  }

  Future<void> updateCall(Match match) async {
    Map<Object, Object?> properties = {
      'call':
          match.call.map((Callee callee) => Callee.toFirestore(callee)).toList()
    };

    await getDocRefById(match.id).update(properties);
  }

  Future<List<Match>> getMatchesByEventId(String eventId) async {
    return (await _databaseService.matches
            .where('eventId', isEqualTo: eventId)
            .withConverter(
                fromFirestore: (snapshot, _) => Match.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((e) => e.data())
        .toList();
  }

  Future<void> updateSheet(Match match) async {
    Map<Object, Object?> properties = {'sheet': Sheet.toFirestore(match.sheet)};
    await getDocRefById(match.id).update(properties);
  }
}
