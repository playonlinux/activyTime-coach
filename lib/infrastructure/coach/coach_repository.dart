import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';

class CoachRepository {
  final DatabaseService _databaseService = DatabaseService();

  Future<Coach> getCoachByRef(DocumentReference coachRef) async {
    return (await coachRef
            .withConverter(
                fromFirestore: (snapshot, _) => Coach.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .data()!;
  }

  Future<List<Coach>> findCoachesByIds(List<String> coachIds) async {
    List<Coach> coaches = [];
    await Future.forEach(coachIds.slices(30), (ids) async {
      List<Coach> c = (await _databaseService.coaches
              .where(FieldPath.documentId, whereIn: ids)
              .withConverter(
                  fromFirestore: (snapshot, _) => Coach.fromFirestore(
                      {...snapshot.data()!, 'id': snapshot.id}),
                  toFirestore: (value, options) => {})
              .get())
          .docs
          .map((e) => e.data())
          .toList();
      coaches.addAll(c);
    });
    return coaches;
  }

  Future<List<Coach>> getCoaches() async {
    return (await _databaseService.coaches
            .withConverter(
                fromFirestore: (snapshot, _) => Coach.fromFirestore(
                    {...snapshot.data()!, 'id': snapshot.id}),
                toFirestore: (value, options) => {})
            .get())
        .docs
        .map((snapshot) => snapshot.data())
        .where((coach) => coach.groupRefs.isNotEmpty)
        .toList();
  }
}
