import 'dart:core';
import 'package:app_flutter/screens/account/account_screen.dart';
import 'package:app_flutter/screens/category/category_screen.dart';
import 'package:app_flutter/screens/coach/coach_screen.dart';
import 'package:app_flutter/screens/group/group_screen.dart';
import 'package:app_flutter/screens/home/home_screen.dart';
import 'package:app_flutter/screens/login/login_screen.dart';
import 'package:app_flutter/screens/login/signup_screen.dart';
import 'package:app_flutter/screens/match/match_screen.dart';
import 'package:app_flutter/screens/player/player_screen.dart';
import 'package:app_flutter/screens/responsible/responsible_screen.dart';
import 'package:app_flutter/screens/settings/settings_screen.dart';
import 'package:app_flutter/screens/sheet/sheet_screen.dart';
import 'package:app_flutter/screens/training/training_screen.dart';
import 'package:app_flutter/settings/firebase_options.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  await initializeDateFormatting('fr');
  runApp(MainApp(preferences: await SharedPreferences.getInstance()));
}

class MainApp extends StatelessWidget {
  final SharedPreferences preferences;

  const MainApp({super.key, required this.preferences});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => ThemeProvider(preferences, context),
        child:
            Consumer<ThemeProvider>(builder: (context, themeProvider, child) {
          
          return MaterialApp(
            theme: ThemeData(
                useMaterial3: true, colorScheme: themeProvider.lightScheme),
            darkTheme: ThemeData(
                useMaterial3: true, colorScheme: themeProvider.darkScheme),
            themeMode: themeProvider.themeMode,
            debugShowCheckedModeBanner: false,
            routes: {
              '/': (context) => const LoginScreen(),
              '/home': (context) => const HomeScreen(),
              '/training': (context) => const TrainingScreen(),
              '/match': (context) => const MatchScreen(),
              '/sheet': (context) => const SheetScreen(),
              '/player': (context) => const PlayerScreen(),
              '/coach': (context) => const CoachScreen(),
              '/responsible': (context) => const ResponsibleScreen(),
              '/category': (context) => const CategoryScreen(),
              '/group': (context) => const GroupScreen(),
              '/settings': (context) => const SettingsScreen(),
              '/account': (context) => const AccountScreen(),
              '/signup': (context) => const SignUpScreen()
            },
          );
        }));
  }
}



// class Wrapper extends StatelessWidget {
//   const Wrapper({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return const HomeScreen();
//   }
// }

