import 'package:app_flutter/infrastructure/coach/coach_repository.dart';
import 'package:app_flutter/infrastructure/match/match_repository.dart';
import 'package:app_flutter/infrastructure/player/player_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/coach/coach_screen.dart';
import 'package:app_flutter/screens/player/player_screen.dart';
import 'package:app_flutter/screens/sheet/sheet_screen.dart';
import 'package:app_flutter/shared/widgets/call_status_selector/call_status_selector.dart';
import 'package:app_flutter/shared/widgets/comment_dialog/comment_dialog.dart';
import 'package:app_flutter/shared/models/status_selection.dart';
import 'package:app_flutter/shared/widgets/info_dialog/info_dialog.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MatchScreenArguments {
  final Match match;
  final Category category;
  final Group group;
  MatchScreenArguments(
      {required this.match, required this.category, required this.group});
}

class MatchScreen extends StatefulWidget {
  const MatchScreen({super.key});

  @override
  State<MatchScreen> createState() => _MatchScreenState();
}

class _MatchScreenState extends State<MatchScreen> {
  final PlayerRepository _playerRepository = PlayerRepository();
  final MatchRepository _matchRepository = MatchRepository();
  final CoachRepository _coachRepository = CoachRepository();
  List<Player> _players = [];

  Match? _match;
  Coach? _coach;
  bool _isLoading = false;

  Future<List<Player>> getPlayersByRef(
      List<DocumentReference> playerRefs) async {
    if (playerRefs.isEmpty) {
      return [];
    }

    return _playerRepository
        .findPlayersByIds(playerRefs.map((playerRef) => playerRef.id).toList());
  }

  Future<bool> getData() async {
    final matchId =
        (ModalRoute.of(context)!.settings.arguments as MatchScreenArguments)
            .match
            .id;

    Match match = await _matchRepository.getMatchById(matchId);
    List<Player>? calledPlayers =
        await getPlayersByRef(match.sheet.calledPlayerRefs);
    List<Player>? notCalledPlayers =
        await getPlayersByRef(match.sheet.notCalledPlayerRefs);

    Coach coach = await _coachRepository.getCoachByRef(match.coachRef);

    if (mounted) {
      setState(() {
        _players = [...calledPlayers, ...notCalledPlayers];
        _match = match;
        _coach = coach;
      });
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as MatchScreenArguments;
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                      !snapshot.hasData) ||
                  _isLoading,
              color: arguments.match.color.topBar,
              child: Scaffold(
                  backgroundColor: arguments.match.color.background,
                  appBar: AppBar(
                    toolbarHeight: 70,
                    centerTitle: true,
                    foregroundColor: Colors.white,
                    backgroundColor: arguments.match.color.topBar,
                    title: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                            '${arguments.category.label}-${arguments.group.level}'),
                        const SizedBox(
                          width: 10.0,
                        ),
                        if (_match?.meeting != null)
                          Text(
                              '${DateFormat('dd/MM/yyyy').format(_match!.meeting.startTime)} ${DateFormat.Hm().format(_match!.meeting.startTime)} - ${DateFormat.Hm().format(_match!.meeting.endTime)}')
                      ],
                    ),
                    actions: [
                      IconButton(
                        icon: const Icon(Icons.info_outline),
                        onPressed: () {
                          InfoDialog.show(
                            context: context,
                            backgroundColor: arguments.match.color.topBar,
                            children: [
                              ListTile(
                                  leading:
                                      UserAvatar(url: _coach!.profile.avatar),
                                  title: Text(
                                    '${_coach!.profile.firstName} ${_coach!.profile.lastName}',
                                    style: const TextStyle(color: Colors.white),
                                  ),
                                  subtitle: const Text('Coach',
                                      style: TextStyle(color: Colors.white)),
                                  onTap: () {
                                    Navigator.pushNamed(context, '/coach',
                                        arguments: CoachScreenArguments(
                                            coach: _coach!));
                                  }),
                              // Row(
                              //   mainAxisAlignment:
                              //       MainAxisAlignment.spaceBetween,
                              //   children: [
                              //     const Text(
                              //       'Coach',
                              //       style: TextStyle(color: Colors.white),
                              //     ),
                              //     TextLink(
                              //       text:
                              //           '${_coach!.profile.firstName} ${_coach!.profile.lastName}',
                              //       style: const TextStyle(color: Colors.white),
                              //       onClick: () {
                              //         Navigator.pushNamed(context, '/coach',
                              //             arguments: CoachScreenArguments(
                              //                 coach: _coach!));
                              //       },
                              //     )
                              //   ],
                              // ),

                              ListTile(
                                  leading: const Text(
                                    'VS',
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                  title: Text(
                                    arguments.match.opponent.name,
                                    style: const TextStyle(color: Colors.white),
                                  ),
                                  subtitle: const Text(
                                    'Adversaire',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onTap: () {
                                    print('navigation adversaire');
                                  }),

                              ListTile(
                                  leading: const Icon(
                                    Icons.location_on_outlined,
                                    color: Colors.white,
                                  ),
                                  title: Text(
                                    arguments.match.meeting.point,
                                    style: const TextStyle(color: Colors.white),
                                  ),
                                  subtitle: const Text(
                                    'Lieu',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  // subtitle: const Text('Coach',
                                  //     style: TextStyle(color: Colors.white)),
                                  onTap: () {
                                    print('navigation gps');
                                  }),
                            ],
                          );
                        },
                      )
                    ],
                  ),
                  bottomNavigationBar: BottomAppBar(
                      color: arguments.match.color.topBar,
                      child: Row(children: [
                        Expanded(
                            child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            foregroundColor: Colors.white,
                          ),
                          onPressed: ([
                                    EventStatus.idle,
                                    EventStatus.in_progress
                                  ].contains(_match?.status) &&
                                  _match!.sheet.calledPlayerRefs.isNotEmpty)
                              ? () async {
                                  await updateStatus(_match!);
                                }
                              : null,
                          child: Text(
                              _match?.status.getButtonLabelByStatus() ?? ''),
                        )),
                        Expanded(
                            child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            foregroundColor: Colors.white,
                          ),
                          onPressed: ([
                            EventStatus.idle,
                            EventStatus.in_progress
                          ].contains(_match?.status))
                              ? () async {
                                  await Navigator.pushNamed(context, '/sheet',
                                      arguments: SheetScreenArguments(
                                        match: _match!,
                                        category: arguments.category,
                                      )).then((value) => getData());
                                }
                              : null,
                          child: const Text('FEUILLE DE MATCH'),
                        ))
                      ])),
                  body: ChangeNotifierProvider<StatusSelection<CalleeStatus>>(
                      create: (context) => StatusSelection(_match?.call
                              .groupFoldBy<String, CalleeStatus>(
                                  (Callee callee) => callee.playerRef.id,
                                  (CalleeStatus? previous, Callee? element) =>
                                      element?.status ?? CalleeStatus.absent) ??
                          {}),
                      child: ListView.separated(
                          separatorBuilder: (BuildContext context, int index) =>
                              const Divider(),
                          itemCount: _players.length,
                          itemBuilder: (context, index) {
                            return Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5.0, vertical: 5.0),
                                // decoration: const BoxDecoration(
                                //     shape: BoxShape.rectangle,
                                //     //borderRadius: BorderRadius.circular(10),
                                //     border: Border(
                                //         bottom:
                                //             BorderSide(color: Colors.white24))),
                                child: ListTile(
                                    contentPadding: const EdgeInsets.all(0),
                                    leading: SizedBox(
                                      width: 50,
                                      child: UserAvatar(
                                          url: _players[index].profile.avatar),
                                    ),
                                    trailing: null,
                                    title: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        TextLink(
                                          text:
                                              '${_players[index].profile.firstName}\n${_players[index].profile.lastName.split(' ').join('\n').split('-').join('\n')}',
                                          onClick: () {
                                            Navigator.pushNamed(
                                                context, '/player',
                                                arguments:
                                                    PlayerScreenArguments(
                                                        player:
                                                            _players[index]));
                                          },
                                          style: const TextStyle(
                                              color: Colors.white),
                                        ),
                                        Row(children: [
                                          (_match!.sheet.calledPlayerRefs.any(
                                                  (DocumentReference
                                                          calledPlayerRef) =>
                                                      calledPlayerRef.id ==
                                                      _players[index].id))
                                              ? CallStatusSelector(
                                                  status: _match!.status,
                                                  color: _match!.color.topBar,
                                                  backgroundColor:
                                                      _match!.color.background,
                                                  id: _players[index].id,
                                                  onStatusChanged: (CalleeStatus
                                                      status) async {
                                                    await updateCall(
                                                        _match!,
                                                        _players[index],
                                                        status);
                                                  },
                                                )
                                              : const Text(
                                                  'Non convoqué',
                                                  style: TextStyle(
                                                      color: Colors.white70),
                                                  textAlign: TextAlign.start,
                                                ),
                                          IconButton(
                                              icon: const Icon(
                                                  Icons.comment_rounded),
                                              color: (_players[index]
                                                      .statistics
                                                      .comments
                                                      .any((comment) =>
                                                          comment.eventType ==
                                                              EventType.match &&
                                                          comment.eventRef.id ==
                                                              _match?.id))
                                                  ? Colors.amber
                                                  : Colors.white,
                                              onPressed: () {
                                                CommentDialog.show(
                                                    context: context,
                                                    player: _players[index],
                                                    eventType: EventType.match,
                                                    eventId: arguments.match.id,
                                                    backgroundColor: _match!
                                                        .color.background,
                                                    onConfirm: (String
                                                        commentText) async {
                                                      int indexComment = _players[
                                                              index]
                                                          .statistics
                                                          .comments
                                                          .indexWhere((comment) =>
                                                              comment.eventType ==
                                                                  EventType
                                                                      .match &&
                                                              comment.eventRef
                                                                      .id ==
                                                                  arguments
                                                                      .match
                                                                      .id);

                                                      if (indexComment == -1) {
                                                        _players[index]
                                                            .statistics
                                                            .comments
                                                            .add(Comment(
                                                                eventType:
                                                                    EventType
                                                                        .match,
                                                                eventRef: _matchRepository
                                                                    .getDocRefById(
                                                                        arguments
                                                                            .match
                                                                            .id),
                                                                text:
                                                                    commentText));
                                                      } else {
                                                        _players[index]
                                                            .statistics
                                                            .comments[
                                                                indexComment]
                                                            .text = commentText;
                                                      }

                                                      await _playerRepository
                                                          .updateComment(
                                                              _players[index]);
                                                    },
                                                    onDelete: () async {
                                                      int indexComment = _players[
                                                              index]
                                                          .statistics
                                                          .comments
                                                          .indexWhere((comment) =>
                                                              comment.eventType ==
                                                                  EventType
                                                                      .match &&
                                                              comment.eventRef
                                                                      .id ==
                                                                  arguments
                                                                      .match
                                                                      .id);
                                                      if (indexComment != -1) {
                                                        _players[index]
                                                            .statistics
                                                            .comments
                                                            .removeAt(
                                                                indexComment);
                                                        await _playerRepository
                                                            .updateComment(
                                                                _players[
                                                                    index]);
                                                      }
                                                    });
                                              })
                                        ])
                                      ],
                                    )));
                          }))));
        });
  }

  Future<void> updateCall(
      Match match, Player player, CalleeStatus status) async {
    DocumentReference playerRef = _playerRepository.getDocRefById(player.id);
    int playerCalleeIndex =
        match.call.indexWhere((Callee callee) => callee.playerRef == playerRef);
    if (playerCalleeIndex == -1) {
      match.call.add(Callee(playerRef: playerRef, status: status, reason: ''));
    } else {
      match.call[playerCalleeIndex].status = status;
    }
    await _matchRepository.updateCall(match);
  }

  Future<void> updateStatus(Match match) async {
    setState(() {
      _isLoading = true;
    });
    match.status = EventStatus.getNextStatus(match.status);
    await _matchRepository.updateStatus(match);

    if (match.status == EventStatus.ended) {
      await Future.forEach(match.call, (callee) async {
        Player player = _players.firstWhere((p) => p.id == callee.playerRef.id);
        String key = DateFormat('yyyy_MM').format(match.meeting.startTime);
        if (player.statistics.stats[key] == null) {
          player.statistics.stats[key] = PlayerMonthlyStatistics(
              matches: PlayerMonthlyMatchStatistics(),
              trainings: PlayerMonthlyTrainingStatistics());
        }
        switch (callee.status) {
          case CalleeStatus.present:
            player.statistics.stats[key]!.matches.nbPresent++;
          case CalleeStatus.absent:
            player.statistics.stats[key]!.matches.nbAbsent++;
          case CalleeStatus.late:
            player.statistics.stats[key]!.matches.nbLate++;
          case CalleeStatus.injured:
            player.statistics.stats[key]!.matches.nbInjured++;
        }

        player.statistics.stats[key]!.matches.nbMatches++;
        await _playerRepository.updateStats(player);
      });
    }
    setState(() {
      _isLoading = false;
    });
  }
}
