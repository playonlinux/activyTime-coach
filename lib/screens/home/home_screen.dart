import 'package:app_flutter/infrastructure/category/category_repository.dart';
import 'package:app_flutter/infrastructure/group/group_repository.dart';
import 'package:app_flutter/infrastructure/match/match_repository.dart';
import 'package:app_flutter/infrastructure/training/training_repository.dart';
import 'package:app_flutter/infrastructure/user/user_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/match/match_screen.dart';
import 'package:app_flutter/screens/training/training_screen.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}

class HomeScreenArguments {
  final DocumentReference coachRef;
  HomeScreenArguments(this.coachRef);
}

class _HomeScreenState extends State<HomeScreen> {
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  final ValueNotifier<List<CalendarEvent>> _selectedEvents = ValueNotifier([]);

  final TrainingRepository _trainingRepository = TrainingRepository();
  final MatchRepository _matchRepository = MatchRepository();
  final UserRepository _userRepository = UserRepository();

  Map<String, List<CalendarEvent>> _calendar = {};

  final ValueNotifier<bool> _isLoading = ValueNotifier(false);

  Future<bool> getData() async {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as HomeScreenArguments;

    _isLoading.value = true;

    List<Training> trainings =
        await _trainingRepository.getTrainingsByCoachRef(arguments.coachRef);
    List<Match> matches =
        await _matchRepository.getMatchsByCoachRef(arguments.coachRef);

    setState(() {
      _calendar = groupBy<CalendarEvent, String>(
          [...trainings, ...matches],
          (CalendarEvent event) =>
              DateFormat('yyyy_MM_dd').format(event.meeting.startTime));
      _selectedDay = _focusedDay;
      _selectedEvents.value = _getEventsForDay(_selectedDay!);
    });

    _isLoading.value = false;
    return true;
  }

  @override
  void initState() {
    Future.delayed(Duration.zero).then((value) => getData());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
        valueListenable: _isLoading,
        builder: (context, isLoading, child) => LoadingOverlay(
            isLoading: isLoading,
            child: Scaffold(
              appBar: AppBar(
                surfaceTintColor: Colors.transparent,
                automaticallyImplyLeading: false,
                toolbarHeight: 35,
                actions: [
                  IconButton(
                      icon: const Icon(Icons.more_horiz),
                      onPressed: () {
                        showModalBottomSheet(
                            context: context,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4)),
                            showDragHandle: true,
                            builder: (context) {
                              return SingleChildScrollView(
                                  child: Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 20.0),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          ListTile(
                                            leading: const Icon(
                                                Icons.person_outline_sharp),
                                            title: const Text('Mon compte'),
                                            onTap: () => Navigator.pushNamed(
                                                context, '/account'),
                                          ),
                                          ListTile(
                                            leading: const Icon(
                                                Icons.settings_outlined),
                                            title: const Text('Paramètres'),
                                            onTap: () => Navigator.pushNamed(
                                                context, '/settings'),
                                          ),
                                          ListTile(
                                            leading: const Icon(
                                                Icons.exit_to_app_outlined,
                                                color: Colors.redAccent),
                                            title: const Text('Se déconnecter'),
                                            onTap: () async {
                                              await _userRepository
                                                  .signOut()
                                                  .then((value) =>
                                                      Navigator.popAndPushNamed(
                                                          context, '/'));
                                            },
                                          )
                                        ],
                                      )));
                            });
                      })
                ],
              ),
              body: Center(
                child: Column(
                  children: [
                    Material(
                      elevation: 1.0,
                      type: MaterialType.canvas,
                      child: TableCalendar(
                          weekNumbersVisible: true,
                          firstDay: DateTime.utc(2010, 10, 16),
                          lastDay: DateTime.utc(2030, 3, 14),
                          focusedDay: _focusedDay,
                          locale: 'fr_FR',
                          headerStyle: HeaderStyle(
                              leftChevronVisible: false,
                              rightChevronVisible: false,
                              titleCentered: true,
                              titleTextFormatter: (date, locale) =>
                                  DateFormat.yMMMM('fr')
                                      .format(date)
                                      .capitalize()),
                          startingDayOfWeek: StartingDayOfWeek.monday,
                          availableCalendarFormats: const {
                            CalendarFormat.month: 'Month'
                          },
                          pageJumpingEnabled: true,
                          enabledDayPredicate: (day) =>
                              _getEventsForDay(day).isNotEmpty,
                          selectedDayPredicate: (day) =>
                              isSameDay(_selectedDay, day),
                          onDaySelected: (selectedDay, focusedDay) {
                            if (!isSameDay(_selectedDay, selectedDay)) {
                              setState(() {
                                _selectedDay = selectedDay;
                                _focusedDay = focusedDay;
                                _selectedEvents.value =
                                    _getEventsForDay(selectedDay);
                              });
                            }
                          },
                          eventLoader: (DateTime day) => _getEventsForDay(day),
                          calendarStyle: CalendarStyle(
                              selectedDecoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.fromBorderSide(BorderSide(
                                      color: _selectedEvents.value.isNotEmpty
                                          ? _selectedEvents
                                              .value[0].color.topBar
                                          : Colors.transparent))),
                              todayDecoration: const BoxDecoration(
                                color: Colors.transparent,
                                shape: BoxShape.circle,
                              ),
                              todayTextStyle: const TextStyle(
                                  color: Colors.amber,
                                  decorationColor: Colors.transparent),
                              selectedTextStyle: TextStyle(
                                  color: _selectedEvents.value.isNotEmpty
                                      ? _selectedEvents.value[0].color.topBar
                                      : Colors.transparent),
                              //canMarkersOverflow: false,
                              // Use `CalendarStyle` to customize the UI
                              // outsideDaysVisible: false,
                              markersAutoAligned: false,
                              markersOffset: const PositionedOffset(top: 12.0),
                              markersAlignment: Alignment.topCenter,
                              markersMaxCount: 1,
                              markerSize: 3,
                              markerDecoration: BoxDecoration(
                                  color: Theme.of(context).colorScheme.primary,
                                  shape: BoxShape.circle)),
                          daysOfWeekStyle: DaysOfWeekStyle(
                              decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Colors.grey[850]!)),
                              ),
                              dowTextFormatter: (day, locale) => DateFormat.E(locale).format(day).toUpperCase(),
                              weekdayStyle: TextStyle(fontSize: 10.0, color: Colors.grey[600]),
                              weekendStyle: TextStyle(fontSize: 10.0, color: Colors.grey[600])),
                          calendarBuilders: CalendarBuilders(
                            weekNumberBuilder: (context, weekNumber) {
                              return Container(
                                  width: 15.0,
                                  decoration: BoxDecoration(
                                    border: Border(
                                        right: BorderSide(
                                            color: Colors.grey[850]!)),
                                  ),
                                  child: Center(
                                      child: Text('$weekNumber',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.grey[600]))));
                            },
                          )),
                    ),
                    Expanded(
                      child: ValueListenableBuilder<List<CalendarEvent>>(
                        valueListenable: _selectedEvents,
                        builder: (context, events, _) => ListView.builder(
                          padding:
                              const EdgeInsets.fromLTRB(13.5, 10.0, 13.5, 20.0),
                          itemCount: events.length,
                          itemBuilder: (context, index) => Padding(
                              padding: const EdgeInsets.only(
                                bottom: 5.0,
                              ),
                              child: EventCard(event: events[index])),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )));
  }

  List<CalendarEvent> _getEventsForDay(DateTime day) {
    return _calendar[DateFormat('yyyy_MM_dd').format(day)] ?? [];
  }
}

enum ActionMenuItem { account, settings, signout }

class EventCard extends StatefulWidget {
  final CalendarEvent event;
  const EventCard({super.key, required this.event});
  @override
  State<EventCard> createState() => _EventCardState();
}

class _EventCardState extends State<EventCard> {
  Category? _category;
  Group? _group;

  final CategoryRepository _categoryRepository = CategoryRepository();
  final GroupRepository _groupRepository = GroupRepository();

  Future<Category> getCategoryByRef(DocumentReference categoryRef) async {
    return await _categoryRepository
        .getCategoryByRef(categoryRef)
        .catchError((error) {
      throw error;
    });
  }

  Future<Group?> getGroupByRef(String groupId) async {
    return await _groupRepository.getGroupById(groupId).catchError((error) {
      throw error;
    });
  }

  Future<void> getProperties() async {
    Group? group = await getGroupByRef(widget.event.groupRef.id);
    Category? category = await getCategoryByRef(group!.categoryRef);
    if (mounted) {
      setState(() {
        _group = group;
        _category = category;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureProvider.value(
      value: getProperties(),
      initialData: null,
      child: Container(
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(
                color: widget.event.status != EventStatus.in_progress
                    ? widget.event.color.background
                    : widget.event.color.topBar,
                width:
                    widget.event.status == EventStatus.cancelled ? 1.0 : 2.0)),
        child: ListTile(
          enabled: widget.event.status != EventStatus.cancelled,
          onTap: () {
            if (widget.event.type == EventType.training) {
              _goToTrainingScreen(context);
            } else {
              _goToMatchScreen(context);
            }
          },
          leading: SizedBox(
            width: 100,
            child: Text(
              widget.event.type.toLabel(),
              style: TextStyle(
                  color: widget.event.status != EventStatus.in_progress
                      ? widget.event.color.background
                      : widget.event.color.topBar,
                  fontWeight: widget.event.status != EventStatus.in_progress
                      ? FontWeight.normal
                      : FontWeight.bold),
              textAlign: TextAlign.start,
            ),
          ),
          title: Text(
            '${_category?.label}-${_group?.level}',
            style: TextStyle(
                color: widget.event.status != EventStatus.in_progress
                    ? widget.event.color.background
                    : widget.event.color.topBar,
                fontWeight: widget.event.status != EventStatus.in_progress
                    ? FontWeight.normal
                    : FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          subtitle: Text(
            '${DateFormat.Hm().format(widget.event.meeting.startTime)} - ${DateFormat.Hm().format(widget.event.meeting.endTime)}',
            style: TextStyle(
                color: widget.event.status != EventStatus.in_progress
                    ? widget.event.color.background
                    : widget.event.color.topBar,
                fontWeight: widget.event.status != EventStatus.in_progress
                    ? FontWeight.normal
                    : FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          trailing: SizedBox(
            width: 100,
            child: Text(
              widget.event.status.toLabel(),
              style: TextStyle(
                  color: widget.event.status != EventStatus.in_progress
                      ? widget.event.color.background
                      : widget.event.color.topBar,
                  fontWeight: widget.event.status != EventStatus.in_progress
                      ? FontWeight.normal
                      : FontWeight.bold),
              textAlign: TextAlign.end,
            ),
          ),
          visualDensity: VisualDensity.compact,
        ),
      ),
    );
  }

  Future _goToMatchScreen(BuildContext context) async {
    return Navigator.pushNamed(context, '/match',
        arguments: MatchScreenArguments(
            match: widget.event as Match,
            category: _category!,
            group: _group!));
  }

  Future _goToTrainingScreen(BuildContext context) async {
    return Navigator.pushNamed(context, '/training',
        arguments: TrainingScreenArguments(
            training: widget.event as Training,
            category: _category!,
            group: _group!));
  }
}
