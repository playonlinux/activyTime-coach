import 'package:app_flutter/infrastructure/category/category_repository.dart';
import 'package:app_flutter/infrastructure/group/group_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/category/category_screen.dart';
import 'package:app_flutter/screens/group/group_screen.dart';
import 'package:app_flutter/shared/widgets/email_link/email_link.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/phone_link/phone_link.dart';
import 'package:app_flutter/shared/widgets/section_title/section_title.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CoachScreenArguments {
  final Coach coach;

  CoachScreenArguments({required this.coach});
}

class CoachScreen extends StatefulWidget {
  const CoachScreen({super.key});

  @override
  State<CoachScreen> createState() => _CoachScreenState();
}

class _CoachScreenState extends State<CoachScreen> {
  List<Category> _categories = [];
  List<Category> _allCategories = [];
  List<Group> _groups = [];
  Coach? _coach;

  final CategoryRepository _categoryRepository = CategoryRepository();
  final GroupRepository _groupRepository = GroupRepository();

  Future<bool> getData() async {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as CoachScreenArguments;

    List<Group> groups =
        await _groupRepository.findGroupsByIdsSortedByGroupLevel(
            arguments.coach.groupRefs.map((groupRef) => groupRef.id).toList());
    List<Category> categories = await _categoryRepository
        .findCategoriesByRefs(arguments.coach.categoryRefs);
    List<Category> allCategories =
        await _categoryRepository.findCategoriesByRefs(
            groups.map((group) => group.categoryRef).toList());

    if (mounted) {
      setState(() {
        _categories = categories;
        _allCategories = allCategories;
        _groups = groups;
        _coach = arguments.coach;
      });
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                  !snapshot.hasData),
              child: Scaffold(
                  appBar: AppBar(),
                  body: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Column(
                        children: [
                          Center(
                              child: UserAvatar(
                            url: _coach?.profile.avatar,
                            size: AvatarSize.medium,
                          )),
                          const SectionTitle(title: 'nom'),
                          Text(
                              '${_coach?.profile.firstName} ${_coach?.profile.lastName}'),
                          const SectionTitle(title: 'date de naissance'),
                          Text((_coach?.profile.dateOfBirth != null)
                              ? DateFormat('dd/MM/yyyy')
                                  .format(_coach!.profile.dateOfBirth)
                              : ''),
                          const SectionTitle(title: 'email'),
                          EmailLink(
                              email: _coach?.details.email,
                              onClick: () {
                                print('sendEmail');
                              }),
                          const SectionTitle(title: 'téléphone'),
                          PhoneLink(
                              phone: _coach?.details.phone,
                              onClick: () {
                                print('phone');
                              }),
                          const SectionTitle(title: 'Catégories'),
                          ..._categories.map((category) => TextLink(
                              text: '${category.label}',
                              onClick: () {
                                Navigator.pushNamed(context, '/category',
                                    arguments: CategoryScreenArguments(
                                        category: category));
                              })),
                          const SectionTitle(title: 'groupes'),
                          ..._groups.map((group) {
                            Category category = _allCategories.firstWhere(
                                (c) => c.id == group.categoryRef.id);
                            return TextLink(
                                text: '${category.label}-${group.level}',
                                onClick: () {
                                  Navigator.pushNamed(context, '/group',
                                      arguments:
                                          GroupScreenArguments(group: group));
                                });
                          })
                        ],
                      ))));
        });
  }
}
