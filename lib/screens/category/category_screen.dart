import 'package:app_flutter/infrastructure/coach/coach_repository.dart';
import 'package:app_flutter/infrastructure/group/group_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/coach/coach_screen.dart';
import 'package:app_flutter/screens/group/group_screen.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/section_title/section_title.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

class CategoryScreenArguments {
  final Category category;

  CategoryScreenArguments({required this.category});
}

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({super.key});

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  final GroupRepository _groupRepository = GroupRepository();
  final CoachRepository _coachRepository = CoachRepository();

  Category? _category;
  List<Group> _groups = [];
  List<Coach> _coaches = [];

  Future<bool> getData() async {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as CategoryScreenArguments;

    List<Group> groups = await _groupRepository
        .findGroupsByIdsSortedByGroupLevel(arguments.category.groupRefs
            .map((groupRef) => groupRef.id)
            .toList());

    List<Coach> coaches = await _coachRepository.findCoachesByIds(groups
        .map((group) => group.coachRefs)
        .flattened
        .map((groupRef) => groupRef.id)
        .toList());

    if (mounted) {
      setState(() {
        _category = arguments.category;
        _groups = groups;
        _coaches = coaches;
      });
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                  !snapshot.hasData),
              child: Scaffold(
                  appBar: AppBar(
                    surfaceTintColor: Colors.transparent,
                    title: Text('${_category?.label}'),
                    centerTitle: true,
                  ),
                  body: SingleChildScrollView(
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Column(children: [
                          const SectionTitle(title: 'coachs'),
                          ...ListTile.divideTiles(
                              context: context,
                              color: Colors.amber[100],
                              tiles: _coaches.map((coach) => ListTile(
                                  leading: UserAvatar(
                                    url: coach.profile.avatar,
                                  ),
                                  title: Text(
                                      '${coach.profile.firstName} ${coach.profile.lastName}'),
                                  onTap: () => Navigator.pushNamed(
                                      context, '/coach',
                                      arguments: CoachScreenArguments(
                                          coach: coach))))),
                          const SectionTitle(title: 'groupes'),
                          GridView.count(
                              shrinkWrap: true,
                              crossAxisCount: 2,
                              children: _groups
                                  .map((group) => Container(
                                        margin: const EdgeInsets.all(50.0),
                                        child: OutlinedButton(
                                            style: OutlinedButton.styleFrom(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18.0),
                                              ),
                                              side: BorderSide(
                                                  width: 2,
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .primary),
                                            ),
                                            onPressed: () => Navigator
                                                .pushNamed(context, '/group',
                                                    arguments:
                                                        GroupScreenArguments(
                                                            group: group)),
                                            child: TextLink(
                                                text: '${group.level}',
                                                onClick: () =>
                                                    Navigator.pushNamed(
                                                        context, '/group',
                                                        arguments:
                                                            GroupScreenArguments(
                                                                group: group)),
                                                style: const TextStyle(
                                                    fontSize: 30))),
                                      ))
                                  .toList())
                        ])),
                  )));
        });
  }
}
