import 'package:app_flutter/infrastructure/category/category_repository.dart';
import 'package:app_flutter/infrastructure/coach/coach_repository.dart';
import 'package:app_flutter/infrastructure/group/group_repository.dart';
import 'package:app_flutter/infrastructure/responsible/responsible_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/category/category_screen.dart';
import 'package:app_flutter/screens/coach/coach_screen.dart';
import 'package:app_flutter/screens/group/group_screen.dart';
import 'package:app_flutter/screens/responsible/responsible_screen.dart';
import 'package:app_flutter/shared/widgets/email_link/email_link.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/phone_link/phone_link.dart';
import 'package:app_flutter/shared/widgets/section_title/section_title.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PlayerScreenArguments {
  final Player player;

  PlayerScreenArguments({required this.player});
}

class PlayerScreen extends StatefulWidget {
  const PlayerScreen({super.key});

  @override
  State<PlayerScreen> createState() => _PlayerScreenState();
}

class _PlayerScreenState extends State<PlayerScreen> {
  Player? _player;
  Category? _category;
  Group? _group;
  Coach? _coach;
  List<Responsible> _responsibles = [];
  List<MapEntry<String, PlayerMonthlyStatistics>> _statistics = [];

  final CoachRepository _coachRepository = CoachRepository();
  final CategoryRepository _categoryRepository = CategoryRepository();
  final GroupRepository _groupRepository = GroupRepository();
  final ResponsibleRepository _responsibleRepository = ResponsibleRepository();

  Future<bool> getData() async {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as PlayerScreenArguments;

    Group group =
        await _groupRepository.getGroupByRef(arguments.player.groupRef);
    Category category =
        await _categoryRepository.getCategoryByRef(group.categoryRef);

    Coach coach = await _coachRepository.getCoachByRef(group.coachRefs[0]);

    List<Responsible> responsibles = await _responsibleRepository
        .findResponsiblesByIds(arguments.player.responsibleRefs
            .map((responsibleRef) => responsibleRef.id)
            .toList());

    if (mounted) {
      setState(() {
        _player = arguments.player;
        _category = category;
        _group = group;
        _coach = coach;
        _responsibles = responsibles;
        _statistics = arguments.player.statistics.stats.entries
            .sortedBy((entry) => entry.key)
            .toList();
      });
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                  !snapshot.hasData),
              child: Scaffold(
                  appBar: AppBar(
                    surfaceTintColor: Colors.transparent,
                  ),
                  body: SingleChildScrollView(
                      //fit: FlexFit.tight,
                      child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: [
                        Center(
                            child: UserAvatar(
                          url: _player?.profile.avatar,
                          size: AvatarSize.medium,
                        )),
                        const SectionTitle(title: 'nom'),
                        Text(
                            '${_player?.profile.firstName} ${_player?.profile.lastName}'),
                        const SectionTitle(title: 'date de naissance'),
                        Text((_player?.profile.dateOfBirth != null)
                            ? DateFormat('dd/MM/yyyy')
                                .format(_player!.profile.dateOfBirth)
                            : ''),
                        const SectionTitle(title: 'email'),
                        EmailLink(
                            email: _player?.details.email,
                            onClick: () {
                              print('sendEmail');
                            }),
                        const SectionTitle(title: 'téléphone'),
                        PhoneLink(
                            phone: _player?.details.phone,
                            onClick: () {
                              print('phone');
                            }),
                        const SectionTitle(title: 'CATÉGORIE'),
                        TextLink(
                            text: '${_category?.label}',
                            onClick: () {
                              Navigator.pushNamed(context, '/category',
                                  arguments: CategoryScreenArguments(
                                      category: _category!));
                            }),
                        const SectionTitle(title: 'GROUPE'),
                        TextLink(
                            text: '${_group?.level}',
                            onClick: () {
                              Navigator.pushNamed(context, '/group',
                                  arguments:
                                      GroupScreenArguments(group: _group!));
                            }),
                        const SectionTitle(title: 'COACH'),
                        ListTile(
                            leading: UserAvatar(
                              url: _coach?.profile.avatar,
                            ),
                            title: Text(
                                '${_coach?.profile.firstName} ${_coach?.profile.lastName}'),
                            onTap: () => Navigator.pushNamed(context, '/coach',
                                arguments:
                                    CoachScreenArguments(coach: _coach!))),
                        const SectionTitle(title: 'RESPONSABLES'),
                        ...ListTile.divideTiles(
                            context: context,
                            color: Colors.amber[100],
                            tiles: _responsibles.map((responsible) => ListTile(
                                leading: UserAvatar(
                                  url: responsible.profile.avatar,
                                ),
                                title: Text(
                                    '${responsible.profile.firstName} ${responsible.profile.lastName}'),
                                onTap: () => Navigator.pushNamed(
                                    context, '/responsible',
                                    arguments: ResponsibleScreenArguments(
                                        responsible: responsible))))),
                        const SectionTitle(title: 'STATISTIQUES'),
                        SizedBox(
                            height: 350,
                            width: double.infinity,
                            child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: _statistics.length,
                              itemBuilder: (context, index) => SizedBox(
                                width: 300,
                                child: Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(DateFormat.yMMMM().format(
                                                DateTime(
                                                    int.parse(
                                                        _statistics[index]
                                                            .key
                                                            .split('_')[0]),
                                                    int.parse(_statistics[index]
                                                        .key
                                                        .split('_')[1]))))
                                          ]),
                                      const Text('Matchs'),
                                      //const RadarChartSample1(),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Convocations'),
                                          Text(
                                              '${_statistics[index].value.matches.nbCalled}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Non convocations'),
                                          Text(
                                              '${_statistics[index].value.matches.nbNotCalled}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Présences'),
                                          Text(
                                              '${_statistics[index].value.matches.nbPresent}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Absences'),
                                          Text(
                                              '${_statistics[index].value.matches.nbAbsent}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Retard'),
                                          Text(
                                              '${_statistics[index].value.matches.nbLate}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Blessures'),
                                          Text(
                                              '${_statistics[index].value.matches.nbInjured}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Total'),
                                          Text(
                                              '${_statistics[index].value.matches.nbMatches}')
                                        ],
                                      ),
                                      const Text('Entraînements'),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Présences'),
                                          Text(
                                              '${_statistics[index].value.trainings.nbPresent}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Absences'),
                                          Text(
                                              '${_statistics[index].value.trainings.nbAbsent}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Retard'),
                                          Text(
                                              '${_statistics[index].value.trainings.nbLate}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Blessures'),
                                          Text(
                                              '${_statistics[index].value.trainings.nbInjured}')
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Total'),
                                          Text(
                                              '${_statistics[index].value.trainings.nbTrainings}')
                                        ],
                                      ),
                                    ]),
                                  ),
                                ),
                              ),
                            )),
                      ],
                    ),
                  ))));
        });
  }
}
