import 'package:app_flutter/infrastructure/player/player_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/player/player_screen.dart';
import 'package:app_flutter/shared/widgets/email_link/email_link.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/phone_link/phone_link.dart';
import 'package:app_flutter/shared/widgets/section_title/section_title.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ResponsibleScreenArguments {
  final Responsible responsible;

  ResponsibleScreenArguments({required this.responsible});
}

class ResponsibleScreen extends StatefulWidget {
  const ResponsibleScreen({super.key});

  @override
  State<ResponsibleScreen> createState() => _ResponsibleScreenState();
}

class _ResponsibleScreenState extends State<ResponsibleScreen> {
  final PlayerRepository responsibleRepository = PlayerRepository();
  List<Player> _players = [];
  Responsible? _responsible;

  Future<bool> getData() async {
    final arguments = ModalRoute.of(context)!.settings.arguments
        as ResponsibleScreenArguments;

    List<Player> players = await responsibleRepository.findPlayersByIds(
        arguments.responsible.playerRefs
            .map((playerRef) => playerRef.id)
            .toList());

    if (mounted) {
      setState(() {
        _responsible = arguments.responsible;
        _players = players;
      });
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                  !snapshot.hasData),
              child: Scaffold(
                  appBar: AppBar(),
                  body: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Column(
                        children: [
                          Center(
                              child: UserAvatar(
                            url: _responsible?.profile.avatar,
                            size: AvatarSize.medium,
                          )),
                          const SectionTitle(title: 'nom'),
                          Text(
                              '${_responsible?.profile.firstName} ${_responsible?.profile.lastName}'),
                          const SectionTitle(title: 'date de naissance'),
                          Text((_responsible?.profile.dateOfBirth != null)
                              ? DateFormat('dd/MM/yyyy')
                                  .format(_responsible!.profile.dateOfBirth)
                              : ''),
                          const SectionTitle(title: 'email'),
                          EmailLink(
                              email: _responsible?.details.email,
                              onClick: () {
                                print('sendEmail');
                              }),
                          const SectionTitle(title: 'téléphone'),
                          PhoneLink(
                              phone: _responsible?.details.phone,
                              onClick: () {
                                print('phone');
                              }),
                          const SectionTitle(title: 'joueurs'),
                          ...ListTile.divideTiles(
                            color: Colors.amber[100],
                            context: context,
                            tiles: _players.map((player) => ListTile(
                                leading: UserAvatar(url: player.profile.avatar),
                                title: Text(
                                    '${player.profile.firstName} ${player.profile.lastName}'),
                                //trailing: const Text('MO'),
                                onTap: () => Navigator.pushNamed(
                                    context, '/player',
                                    arguments: PlayerScreenArguments(
                                        player: player)))),
                          )
                        ],
                      ))));
        });
  }
}
