import 'package:app_flutter/infrastructure/coach/coach_repository.dart';
import 'package:app_flutter/infrastructure/user/user_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/home/home_screen.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final CoachRepository _coachRepository = CoachRepository();
  final UserRepository _userRepository = UserRepository();
  List<String> _users = [];
  final ValueNotifier<String> _selectedEmail = ValueNotifier('');

  getUsers() async {
    List<Coach> coaches = await _coachRepository.getCoaches();
    setState(() {
      _users = coaches.map((coach) => coach.details.email).toList();
      _selectedEmail.value = _users[0];
    });
  }

  @override
  void initState() {
    super.initState();
    getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<String>(
        valueListenable: _selectedEmail,
        builder: (context, isLoading, child) => Scaffold(
                body: Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
              ),
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    DropdownButton(
                      value: _selectedEmail.value,
                      items: _users.map((user) {
                        return DropdownMenuItem(
                          value: user,
                          child: Text(user),
                        );
                      }).toList(),
                      onChanged: (item) {
                        setState(() {
                          _selectedEmail.value = item as String;
                        });
                      },
                    ),
                    OutlinedButton(
                        onPressed: () async {
                          await _userRepository
                              .signInWithEmailAndPassword(
                                  _selectedEmail.value, 'activyTime')
                              .then((user) =>
                                  _userRepository.getUserRefByUid(user!.uid))
                              .then((user) => Navigator.pushReplacementNamed(
                                  context, '/home',
                                  arguments:
                                      HomeScreenArguments(user.userRef)));
                        },
                        child: const Text('Se connecter')),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Pas encore de compte ?'),
                        TextButton(
                          onPressed: () {
                            Navigator.pushReplacementNamed(context, '/signup');
                          },
                          child: const Text('Créez-en un !'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )));
  }
}
