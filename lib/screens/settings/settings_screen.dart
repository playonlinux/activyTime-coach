import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: arguments.training.color.background,
        appBar: AppBar(
          title: const Text('PARAMÈTRES'),
        ),
        body: Column(
          children: [
            ListTile(
              title: const Text('Thème'),
              subtitle: const Text('Supporting text'),
              trailing: CupertinoSwitch(
                value: Theme.of(context).brightness == Brightness.dark,
                onChanged: (bool isDarkMode) {
                  Provider.of<ThemeProvider>(context, listen: false)
                      .setThemeMode(
                          isDarkMode ? ThemeMode.dark : ThemeMode.light);
                },
              ),
            ),
          ],
        ));
  }
}

class ThemeProvider with ChangeNotifier {
  late ThemeMode _themeMode = preferences.getString('theme') != null
      ? ThemeMode.values.byName(preferences.getString('theme')!)
      : ThemeMode.system;
  late ColorScheme _darkScheme = darkColorScheme;
  late ColorScheme _lightScheme = lightColorScheme;

  final SharedPreferences preferences;
  final BuildContext context;

  ThemeProvider(this.preferences, this.context);

  ThemeMode get themeMode => _themeMode;

  bool get isDarkMode => themeMode == ThemeMode.dark;
  void setThemeMode(ThemeMode value) async {
    _themeMode = value;
    preferences.setString('theme', _themeMode.name.toString());
    notifyListeners();
  }

  ColorScheme get darkScheme => _darkScheme;
  void setDarkScheme(ColorScheme value) {
    _darkScheme = value;
    notifyListeners();
  }

  ColorScheme get lightScheme => _lightScheme;
  void setLightScheme(ColorScheme value) {
    _lightScheme = value;
    notifyListeners();
  }

  ColorScheme get currentScheme => isDarkMode ? _darkScheme : _lightScheme;
}

ColorScheme lightColorScheme = const ColorScheme(
    brightness: Brightness.light,
    primary: Color(0xff202020),
    onPrimary: Color(0xff505050),
    primaryContainer: Color(0xff202020),
    onPrimaryContainer: Color(0xff505050),
    secondary: Color(0xffbbbbbb),
    onSecondary: Color(0xffeaeaea),
    secondaryContainer: Color(0xffbbbbbb),
    onSecondaryContainer: Color(0xffeaeaea),
    tertiary: Color(0xffbbbbbb),
    onTertiary: Color(0xffeaeaea),
    tertiaryContainer: Color(0xffbbbbbb),
    onTertiaryContainer: Color(0xffeaeaea),
    error: Color(0xfff32424),
    onError: Color(0xfff32424),
    errorContainer: Color(0xfff32424),
    onErrorContainer: Color(0xfff32424),
    background: Color(0xfff1f2f3),
    onBackground: Color(0xffffffff),
    surface: Color(0xfff1f2f3), //Color(0xff54b435),
    onSurface: Color(0xff202020),
    surfaceVariant: Colors.white, //Color(0xff54b435),
    onSurfaceVariant: Color(0xff202020), // Color(0xffe91e63),
    outline: Color(0xff202020),
    outlineVariant: Colors.white24,
    inverseSurface: Colors.white, //Color(0xffe91e63),
    onInverseSurface: Color(0xff54b435),
    inversePrimary: Color(0xff505050),
    surfaceTint: Color(0xff202020));

ColorScheme darkColorScheme = const ColorScheme(
    brightness: Brightness.dark,
    primary: Colors.white,
    onPrimary: Color(0xff381e72),
    primaryContainer: Color(0xff4f378b),
    onPrimaryContainer: Color(0xffeaddff),
    secondary: Color(0xffccc2dc),
    onSecondary: Color(0xff332d41),
    secondaryContainer: Color(0xff4a4458),
    onSecondaryContainer: Color(0xffe8def8),
    tertiary: Color(0xffefb8c8),
    onTertiary: Color(0xff492532),
    tertiaryContainer: Color(0xff633b48),
    onTertiaryContainer: Color(0xffffd8e4),
    error: Color(0xfff2b8b5),
    onError: Color(0xff601410),
    errorContainer: Color(0xff8c1d18),
    onErrorContainer: Color(0xfff9dedc),
    background: Color(0xff1c1b1f),
    onBackground: Color(0xffe6e1e5),
    surface: Color(0xff1c1b1f),
    onSurface: Color(0xffe6e1e5),
    surfaceVariant: Color(0xff49454f),
    onSurfaceVariant: Color(0xffcac4d0),
    outline: Color(0xff938f99),
    outlineVariant: Colors.white24,
    inverseSurface: Color(0xffe6e1e5),
    onInverseSurface: Color(0xff313033),
    inversePrimary: Color(0xff6750a4),
    surfaceTint: Color(0xffd0bcff));
