import 'package:app_flutter/infrastructure/category/category_repository.dart';
import 'package:app_flutter/infrastructure/coach/coach_repository.dart';
import 'package:app_flutter/infrastructure/player/player_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/category/category_screen.dart';
import 'package:app_flutter/screens/coach/coach_screen.dart';
import 'package:app_flutter/screens/player/player_screen.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/section_title/section_title.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:flutter/material.dart';

class GroupScreenArguments {
  final Group group;

  GroupScreenArguments({required this.group});
}

class GroupScreen extends StatefulWidget {
  const GroupScreen({super.key});

  @override
  State<GroupScreen> createState() => _GroupScreenState();
}

class _GroupScreenState extends State<GroupScreen> {
  Category? _category;
  Group? _group;
  Coach? _coach;
  List<Player> _players = [];

  final CoachRepository _coachRepository = CoachRepository();
  final CategoryRepository _categoryRepository = CategoryRepository();
  final PlayerRepository _playerRepository = PlayerRepository();

  Future<bool> getData() async {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as GroupScreenArguments;

    Category category =
        await _categoryRepository.getCategoryByRef(arguments.group.categoryRef);

    Coach coach =
        await _coachRepository.getCoachByRef(arguments.group.coachRefs[0]);

    List<Player> players = await _playerRepository.findPlayersByIds(
        arguments.group.playerRefs.map((playerRef) => playerRef.id).toList());
    if (mounted) {
      setState(() {
        _category = category;
        _group = arguments.group;
        _coach = coach;
        _players = players;
      });
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                  !snapshot.hasData),
              child: Scaffold(
                  appBar: AppBar(
                    surfaceTintColor: Colors.transparent,
                    centerTitle: true,
                    title: Text('${_category?.label}-${_group?.level}'),
                  ),
                  body: SingleChildScrollView(
                    child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: [
                            const SectionTitle(title: 'catégories'),
                            TextLink(
                                text: '${_category?.label}',
                                onClick: () {
                                  Navigator.pushNamed(context, '/category',
                                      arguments: CategoryScreenArguments(
                                          category: _category!));
                                }),
                            const SectionTitle(title: 'coach'),
                            ListTile(
                                leading: UserAvatar(
                                  url: _coach?.profile.avatar,
                                ),
                                title: Text(
                                    '${_coach?.profile.firstName} ${_coach?.profile.lastName}'),
                                onTap: () => Navigator.pushNamed(
                                    context, '/coach',
                                    arguments:
                                        CoachScreenArguments(coach: _coach!))),
                            const SectionTitle(title: 'joueurs'),
                            ...ListTile.divideTiles(
                              color: Colors.amber[100],
                              context: context,
                              tiles: _players.map(
                                (player) => ListTile(
                                    leading:
                                        UserAvatar(url: player.profile.avatar),
                                    title: Text(
                                        '${player.profile.firstName} ${player.profile.lastName}'),
                                    trailing: const Text('MO'),
                                    onTap: () => Navigator.pushNamed(
                                        context, '/player',
                                        arguments: PlayerScreenArguments(
                                            player: player))),
                              ),
                            ),
                          ],
                        )),
                  )));
        });
  }
}
