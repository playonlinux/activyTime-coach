import 'package:app_flutter/infrastructure/group/group_repository.dart';
import 'package:app_flutter/infrastructure/match/match_repository.dart';
import 'package:app_flutter/infrastructure/player/player_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/player/player_screen.dart';
import 'package:app_flutter/shared/widgets/info_dialog/info_dialog.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/multi_state_toggle/multi_state_toggle.dart';
import 'package:app_flutter/shared/models/status_selection.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SheetScreenArguments {
  final Match match;
  final Category category;
  SheetScreenArguments({required this.match, required this.category});
}

class SheetScreen extends StatefulWidget {
  const SheetScreen({super.key});

  @override
  State<SheetScreen> createState() => _SheetScreenState();
}

class _SheetScreenState extends State<SheetScreen> {
  List<Player> _players = [];
  List<Match> _matches = [];
  List<Group> _groups = [];
  bool _isLoading = false;

  final PlayerRepository _playerRepository = PlayerRepository();
  final MatchRepository _matchRepository = MatchRepository();
  final GroupRepository _groupRepository = GroupRepository();

  Future<List<Player>> _getPlayersByIds(List<String> playerIds) async {
    if (playerIds.isEmpty) {
      return [];
    }

    return _playerRepository.findPlayersByIds(playerIds);
  }

  Future<bool> getData() async {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as SheetScreenArguments;

    List<Match> matches =
        await _matchRepository.getMatchesByEventId(arguments.match.eventId);

    List<Group> groups = await _groupRepository
        .findGroupsByIdsSortedByGroupLevel(arguments.category.groupRefs
            .map((groupRef) => groupRef.id)
            .toList());

    List<Player>? players = (await _getPlayersByIds(groups
            .map((group) => group.playerRefs.map((playerRef) => playerRef.id))
            .flattened
            .toList()))
        .sorted((p1, p2) {
      Group groupP1 = groups.firstWhere((group) => group.id == p1.groupRef.id);
      Group groupP2 = groups.firstWhere((group) => group.id == p2.groupRef.id);

      return groupP1.level.compareTo(groupP2.level);
    });
    if (mounted) {
      setState(() {
        _players = players;
        _matches = matches;
        _groups = groups;
      });
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as SheetScreenArguments;
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                      !snapshot.hasData) ||
                  _isLoading,
              color: arguments.match.color.topBar,
              child: ChangeNotifierProvider<StatusSelection<SheetSelection>>(
                  create: (context) => createStatusSelection(),
                  builder: (context, child) => Scaffold(
                      backgroundColor: arguments.match.color.background,
                      appBar: AppBar(
                          toolbarHeight: 70,
                          centerTitle: true,
                          foregroundColor: Colors.white,
                          backgroundColor: arguments.match.color.topBar,
                          title: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('${arguments.category.label}'),
                              const SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                  '${DateFormat('dd/MM/yyyy').format(arguments.match.meeting.startTime)} ${DateFormat.Hm().format(arguments.match.meeting.startTime)} - ${DateFormat.Hm().format(arguments.match.meeting.endTime)}')
                            ],
                          ),
                          actions: [
                            IconButton(
                              icon: const Icon(Icons.info_outline),
                              onPressed: () {
                                InfoDialog.show(
                                    context: context,
                                    backgroundColor:
                                        arguments.match.color.topBar,
                                    children: [
                                      const Text(
                                        'INFRMM',
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ]);
                              },
                            )
                          ]),
                      bottomNavigationBar: BottomAppBar(
                          color: arguments.match.color.topBar,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                    child: TextButton(
                                  style: const ButtonStyle(
                                      foregroundColor: MaterialStatePropertyAll(
                                          Colors.white),
                                      backgroundColor: MaterialStatePropertyAll(
                                          Colors.transparent)),
                                  onPressed: () async {
                                    await updateMatchesSheet(
                                            Provider.of<
                                                        StatusSelection<
                                                            SheetSelection>>(
                                                    context,
                                                    listen: false)
                                                .selectedOption,
                                            arguments.match)
                                        .then((value) {
                                      Navigator.pop(context);
                                    });
                                  },
                                  child: const Text('VALIDER'),
                                )),
                              ])),
                      body: ListView.builder(
                          itemCount: _players.length,
                          itemBuilder: (bodyContext, index) {
                            return Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 0.0, vertical: 5.0),
                                decoration: const BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    //borderRadius: BorderRadius.circular(10),
                                    border: Border(
                                        bottom:
                                            BorderSide(color: Colors.white24))),
                                child: ListTile(
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    leading: SizedBox(
                                      width: 50,
                                      child: UserAvatar(
                                          url: _players[index].profile.avatar),
                                    ),
                                    trailing: null,
                                    title: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        TextLink(
                                          text:
                                              '${_players[index].profile.firstName}\n${_players[index].profile.lastName.split(' ').join('\n').split('-').join('\n')}',
                                          onClick: () {
                                            Navigator.pushNamed(
                                                context, '/player',
                                                arguments:
                                                    PlayerScreenArguments(
                                                        player:
                                                            _players[index]));
                                          },
                                          style: const TextStyle(
                                              color: Colors.white),
                                        ),
                                        MultiStateToggle<GroupLevel>(
                                          enabled: true,
                                          states: GroupLevel.values,
                                          stateLabels: GroupLevel.values
                                              .groupFoldBy<GroupLevel?, String>(
                                                  (GroupLevel? element) =>
                                                      element,
                                                  (previous, element) =>
                                                      element.toString()),
                                          selectedOption: Provider.of<
                                                  StatusSelection<
                                                      SheetSelection>>(context)
                                              .selectedOption[
                                                  _players[index].id]!
                                              .groupLevel,
                                          onSelectionChange: (selectedOption) {
                                            Group? group = _groups
                                                .firstWhereOrNull((group) =>
                                                    group.level ==
                                                    selectedOption);

                                            Match initialMatch =
                                                _matches.firstWhere((match) =>
                                                    match.groupRef.id ==
                                                    _players[index]
                                                        .groupRef
                                                        .id);
                                            Match? selectedMatch = _matches
                                                .firstWhereOrNull((match) =>
                                                    match.groupRef.id ==
                                                    group?.id);
                                            SheetSelection selection =
                                                SheetSelection(
                                                    sheetId: selectedMatch !=
                                                            null
                                                        ? selectedMatch.sheet.id
                                                        : initialMatch.sheet.id,
                                                    groupLevel: selectedOption);

                                            Provider.of<
                                                        StatusSelection<
                                                            SheetSelection>>(
                                                    context,
                                                    listen: false)
                                                .updateStatus(
                                                    _players[index].id,
                                                    selection);
                                          },
                                          selectionColor:
                                              arguments.match.color.topBar,
                                          backgroundColor:
                                              arguments.match.color.background,
                                        )
                                      ],
                                    )));
                          }))));
        });
  }

  StatusSelection<SheetSelection> createStatusSelection() {
    return StatusSelection(_players
        .map((player) => player.id)
        .toList()
        .groupFoldBy<String, SheetSelection>((String playerId) => playerId,
            (SheetSelection? previous, String playerId) {
      DocumentReference playerRef = _playerRepository.getDocRefById(playerId);

      Match? match = _matches.firstWhereOrNull(
          (m) => m.sheet.calledPlayerRefs.contains(playerRef));

      if (match != null) {
        return SheetSelection(
            sheetId: match.sheet.id,
            groupLevel: _groups
                .firstWhere((group) => group.id == match.groupRef.id)
                .level);
      }

      Match? match2 = _matches.firstWhereOrNull(
          (m) => m.sheet.notCalledPlayerRefs.contains(playerRef));

      if (match2 != null) {
        return SheetSelection(
            sheetId: match2.sheet.id, groupLevel: GroupLevel.NC);
      }

      Match match3 = _matches.firstWhere((match) =>
          match.groupRef ==
          _players.firstWhere((player) => player.id == playerId).groupRef);
      return SheetSelection(
          sheetId: match3.sheet.id,
          groupLevel: _groups
              .firstWhere((group) => group.id == match3.groupRef.id)
              .level);
    }));
  }

  Future<void> updateMatchesSheet(
      Map<String, SheetSelection> selectedOptions, Match match) async {
    setState(() {
      _isLoading = true;
    });
    await Future.forEach(_matches, (match) async {
      await decreasePlayersStats(match);
      updateCalledPlayers(match, selectedOptions);
      updateNotCalledPlayers(match, selectedOptions);
      await _matchRepository.updateSheet(match);
      await increasePlayersStats(match);
    });
    setState(() {
      _isLoading = false;
    });
  }

  void updateCalledPlayers(
      Match match, Map<String, SheetSelection> selectedOptions) {
    match.sheet.calledPlayerRefs.clear();
    match.sheet.calledPlayerRefs.addAll(selectedOptions.entries
        .where((entry) =>
            entry.value.sheetId == match.sheet.id &&
            entry.value.groupLevel != GroupLevel.NC)
        .map((entry) => _playerRepository.getDocRefById(entry.key))
        .toList());
  }

  void updateNotCalledPlayers(
      Match match, Map<String, SheetSelection> selectedOptions) {
    match.sheet.notCalledPlayerRefs.clear();
    match.sheet.notCalledPlayerRefs.addAll(selectedOptions.entries
        .where((entry) =>
            entry.value.sheetId == match.sheet.id &&
            entry.value.groupLevel == GroupLevel.NC)
        .map((entry) => _playerRepository.getDocRefById(entry.key))
        .toList());
  }

  Future<void> increasePlayersStats(Match match) async {
    await updateCalledPlayersStats(match, (nbCalled) => nbCalled + 1);
    await updateNotCalledPlayersStats(match, (nbNotCalled) => nbNotCalled + 1);
  }

  Future<void> decreasePlayersStats(Match match) async {
    await updateCalledPlayersStats(match, (nbCalled) => nbCalled - 1);
    await updateNotCalledPlayersStats(match, (nbNotCalled) => nbNotCalled - 1);
  }

  Future<void> updateCalledPlayersStats(
      Match match, int Function(int) callback) async {
    await Future.forEach(match.sheet.calledPlayerRefs, (calledPlayerRef) async {
      Player player = _players.firstWhere((p) => p.id == calledPlayerRef.id);
      String key = DateFormat('yyyy_MM').format(match.meeting.startTime);

      if (player.statistics.stats[key] == null) {
        player.statistics.stats[key] = PlayerMonthlyStatistics(
            matches: PlayerMonthlyMatchStatistics(),
            trainings: PlayerMonthlyTrainingStatistics());
      }
      player.statistics.stats[key]!.matches.nbCalled =
          callback(player.statistics.stats[key]!.matches.nbCalled);
      await _playerRepository.updateStats(player);
    });
  }

  Future<void> updateNotCalledPlayersStats(
      Match match, int Function(int) callback) async {
    await Future.forEach(match.sheet.notCalledPlayerRefs,
        (notCalledPlayerRef) async {
      Player player = _players.firstWhere((p) => p.id == notCalledPlayerRef.id);
      String key = DateFormat('yyyy_MM').format(match.meeting.startTime);

      if (player.statistics.stats[key] == null) {
        player.statistics.stats[key] = PlayerMonthlyStatistics(
            matches: PlayerMonthlyMatchStatistics(),
            trainings: PlayerMonthlyTrainingStatistics());
      }
      player.statistics.stats[key]!.matches.nbNotCalled =
          callback(player.statistics.stats[key]!.matches.nbNotCalled);
      await _playerRepository.updateStats(player);
    });
  }
}

class SheetSelection {
  String sheetId;
  GroupLevel groupLevel;

  SheetSelection({required this.sheetId, required this.groupLevel});
}
