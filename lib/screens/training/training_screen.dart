import 'package:app_flutter/infrastructure/coach/coach_repository.dart';
import 'package:app_flutter/infrastructure/player/player_repository.dart';
import 'package:app_flutter/infrastructure/training/training_repository.dart';
import 'package:app_flutter/models/models.dart';
import 'package:app_flutter/screens/coach/coach_screen.dart';
import 'package:app_flutter/screens/player/player_screen.dart';
import 'package:app_flutter/shared/widgets/call_status_selector/call_status_selector.dart';
import 'package:app_flutter/shared/widgets/comment_dialog/comment_dialog.dart';
import 'package:app_flutter/shared/models/status_selection.dart';
import 'package:app_flutter/shared/widgets/info_dialog/info_dialog.dart';
import 'package:app_flutter/shared/widgets/loader/loader.dart';
import 'package:app_flutter/shared/widgets/text_link/text_link.dart';
import 'package:app_flutter/shared/widgets/user_avatar/user_avatar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TrainingScreenArguments {
  final Training training;
  final Category category;
  final Group group;

  TrainingScreenArguments(
      {required this.training, required this.category, required this.group});
}

class TrainingScreen extends StatefulWidget {
  const TrainingScreen({super.key});

  @override
  State<TrainingScreen> createState() => _TrainingScreenState();
}

class _TrainingScreenState extends State<TrainingScreen> {
  List<Player> _players = [];
  late Coach _coach;
  bool _isLoading = false;

  final PlayerRepository _playerRepository = PlayerRepository();
  final TrainingRepository _trainingRepository = TrainingRepository();
  final CoachRepository _coachRepository = CoachRepository();

  Future<bool> getData() async {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as TrainingScreenArguments;
    List<Player>? players = await _playerRepository.findPlayersByIds(
        arguments.group.playerRefs.map((playerRef) => playerRef.id).toList());

    Coach coach =
        await _coachRepository.getCoachByRef(arguments.training.coachRef);

    if (mounted) {
      setState(() {
        _players = players;
        _coach = coach;
      });
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as TrainingScreenArguments;
    return FutureBuilder(
        future: getData(),
        initialData: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return LoadingOverlay(
              isLoading: (snapshot.connectionState == ConnectionState.waiting &&
                      !snapshot.hasData) ||
                  _isLoading,
              color: arguments.training.color.topBar,
              child: Scaffold(
                  backgroundColor: arguments.training.color.background,
                  appBar: AppBar(
                      toolbarHeight: 70,
                      centerTitle: true,
                      foregroundColor: Colors.white,
                      backgroundColor: arguments.training.color.topBar,
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                              '${arguments.category.label}/${arguments.group.level}'),
                          Text(
                              '${DateFormat('dd/MM/yyyy').format(arguments.training.meeting.startTime)} ${DateFormat.Hm().format(arguments.training.meeting.startTime)} - ${DateFormat.Hm().format(arguments.training.meeting.endTime)}')
                        ],
                      ),
                      actions: [
                        IconButton(
                          icon: const Icon(Icons.info_outline),
                          onPressed: () {
                            InfoDialog.show(
                              context: context,
                              backgroundColor: arguments.training.color.topBar,
                              children: [
                                ListTile(
                                    leading:
                                        UserAvatar(url: _coach.profile.avatar),
                                    title: Text(
                                      '${_coach.profile.firstName} ${_coach.profile.lastName}',
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    subtitle: const Text('Coach',
                                        style: TextStyle(color: Colors.white)),
                                    onTap: () {
                                      Navigator.pushNamed(context, '/coach',
                                          arguments: CoachScreenArguments(
                                              coach: _coach));
                                    }),
                                ListTile(
                                    leading: const Icon(
                                      Icons.location_on_outlined,
                                      color: Colors.white,
                                    ),
                                    title: Text(
                                      arguments.training.meeting.point,
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    subtitle: const Text(
                                      'Lieu',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    // subtitle: const Text('Coach',
                                    //     style: TextStyle(color: Colors.white)),
                                    onTap: () {
                                      print('navigation gps');
                                    }),
                              ],
                            );
                          },
                        )
                      ]),
                  bottomNavigationBar: BottomAppBar(
                      color: arguments.training.color.topBar,
                      child: TextButton(
                        style: const ButtonStyle(
                            foregroundColor:
                                MaterialStatePropertyAll(Colors.white),
                            backgroundColor:
                                MaterialStatePropertyAll(Colors.transparent)),
                        onPressed: ([EventStatus.idle, EventStatus.in_progress]
                                .contains(arguments.training.status))
                            ? () async {
                                await updateStatus(arguments.training);
                              }
                            : null,
                        child: Text(
                            arguments.training.status.getButtonLabelByStatus()),
                      )),
                  body: ChangeNotifierProvider<StatusSelection<CalleeStatus>>(
                      create: (context) => StatusSelection(arguments
                          .training.call
                          .groupFoldBy<String, CalleeStatus>(
                              (Callee callee) => callee.playerRef.id,
                              (CalleeStatus? previous, Callee? element) =>
                                  element?.status ?? CalleeStatus.absent)),
                      child: ListView.separated(
                          separatorBuilder: (BuildContext context, int index) =>
                              const Divider(),
                          itemCount: _players.length,
                          itemBuilder: (context, index) {
                            return Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5.0, vertical: 5.0),
                                // decoration: const BoxDecoration(
                                //     shape: BoxShape.rectangle,
                                //     // borderRadius: BorderRadius.circular(10),
                                //     border: Border(
                                //         bottom:
                                //             BorderSide(color: Colors.white24))),
                                child: ListTile(
                                    contentPadding: const EdgeInsets.all(0),
                                    leading: SizedBox(
                                      width: 50,
                                      child: UserAvatar(
                                          url: _players[index].profile.avatar),
                                    ),
                                    title: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          TextLink(
                                            text:
                                                '${_players[index].profile.firstName}\n${_players[index].profile.lastName.split(' ').join('\n').split('-').join('\n')}',
                                            onClick: () {
                                              Navigator.pushNamed(
                                                  context, '/player',
                                                  arguments:
                                                      PlayerScreenArguments(
                                                          player:
                                                              _players[index]));
                                            },
                                            style: const TextStyle(
                                                color: Colors.white),
                                          ),
                                          Row(
                                            children: [
                                              CallStatusSelector(
                                                status:
                                                    arguments.training.status,
                                                color: arguments
                                                    .training.color.topBar,
                                                backgroundColor: arguments
                                                    .training.color.background,
                                                id: _players[index].id,
                                                onStatusChanged: (CalleeStatus
                                                    status) async {
                                                  await updateCall(
                                                      arguments.training,
                                                      _players[index],
                                                      status);
                                                },
                                              ),
                                              IconButton(
                                                icon: const Icon(
                                                    Icons.comment_rounded),
                                                color: (_players[index]
                                                        .statistics
                                                        .comments
                                                        .any((comment) =>
                                                            comment.eventType ==
                                                                EventType
                                                                    .training &&
                                                            comment.eventRef
                                                                    .id ==
                                                                arguments
                                                                    .training
                                                                    .id))
                                                    ? Colors.amber
                                                    : Colors.white,
                                                onPressed: () {
                                                  CommentDialog.show(
                                                      context: context,
                                                      player: _players[index],
                                                      eventType:
                                                          EventType.training,
                                                      eventId:
                                                          arguments.training.id,
                                                      backgroundColor: arguments
                                                          .training
                                                          .color
                                                          .background,
                                                      onConfirm: (String
                                                          commentText) async {
                                                        int indexComment = _players[
                                                                index]
                                                            .statistics
                                                            .comments
                                                            .indexWhere((comment) =>
                                                                comment.eventType ==
                                                                    EventType
                                                                        .training &&
                                                                comment.eventRef
                                                                        .id ==
                                                                    arguments
                                                                        .training
                                                                        .id);
                                                        if (indexComment ==
                                                            -1) {
                                                          _players[index]
                                                              .statistics
                                                              .comments
                                                              .add(Comment(
                                                                  eventType: EventType
                                                                      .training,
                                                                  eventRef: _trainingRepository
                                                                      .getDocRefById(arguments
                                                                          .training
                                                                          .id),
                                                                  text:
                                                                      commentText));
                                                        } else {
                                                          _players[index]
                                                              .statistics
                                                              .comments[
                                                                  indexComment]
                                                              .text = commentText;
                                                        }
                                                        await _playerRepository
                                                            .updateComment(
                                                                _players[
                                                                    index]);
                                                      },
                                                      onDelete: () async {
                                                        int indexComment = _players[
                                                                index]
                                                            .statistics
                                                            .comments
                                                            .indexWhere((comment) =>
                                                                comment.eventType ==
                                                                    EventType
                                                                        .training &&
                                                                comment.eventRef
                                                                        .id ==
                                                                    arguments
                                                                        .training
                                                                        .id);
                                                        if (indexComment !=
                                                            -1) {
                                                          _players[index]
                                                              .statistics
                                                              .comments
                                                              .removeAt(
                                                                  indexComment);
                                                          await _playerRepository
                                                              .updateComment(
                                                                  _players[
                                                                      index]);
                                                        }
                                                      });
                                                },
                                              ),
                                            ],
                                          )
                                        ])));
                          }))));
        });
  }

  Future<void> updateCall(
      Training training, Player player, CalleeStatus status) async {
    DocumentReference playerRef = _playerRepository.getDocRefById(player.id);
    int playerCalleeIndex = training.call
        .indexWhere((Callee callee) => callee.playerRef == playerRef);
    if (playerCalleeIndex == -1) {
      training.call
          .add(Callee(playerRef: playerRef, status: status, reason: ''));
    } else {
      training.call[playerCalleeIndex].status = status;
    }

    await _trainingRepository.updateCall(training);
  }

  Future<void> updateStatus(Training training) async {
    setState(() {
      _isLoading = true;
    });
    training.status = EventStatus.getNextStatus(training.status);
    await _trainingRepository.updateStatus(training);
    if (training.status == EventStatus.ended) {
      await Future.forEach(training.call, (callee) async {
        Player player = _players.firstWhere((p) => p.id == callee.playerRef.id);
        String key = DateFormat('yyyy_MM').format(training.meeting.startTime);

        if (player.statistics.stats[key] == null) {
          player.statistics.stats[key] = PlayerMonthlyStatistics(
              matches: PlayerMonthlyMatchStatistics(),
              trainings: PlayerMonthlyTrainingStatistics());
        }
        switch (callee.status) {
          case CalleeStatus.present:
            player.statistics.stats[key]!.trainings.nbPresent++;
          case CalleeStatus.absent:
            player.statistics.stats[key]!.trainings.nbAbsent++;
          case CalleeStatus.late:
            player.statistics.stats[key]!.trainings.nbLate++;
          case CalleeStatus.injured:
            player.statistics.stats[key]!.trainings.nbInjured++;
        }

        player.statistics.stats[key]!.trainings.nbTrainings++;
        await _playerRepository.updateStats(player);
      });
    }
    setState(() {
      _isLoading = false;
    });
  }
}
